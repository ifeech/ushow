package ru.surf.ushow.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import ru.surf.ushow.model.Movie;
import ru.surf.ushow.model.MovieSeason;
import ru.surf.ushow.model.MovieSerial;

public class DBHelper extends OrmLiteSqliteOpenHelper {

    private static final String TAG = DBHelper.class.getSimpleName();

    //имя файла базы данных который будет храниться в /data/data/APPNAME/DATABASE_NAME.db
    private static final String DB_NAME = "uShow.db";

    //с каждым увеличением версии, при нахождении в устройстве БД с предыдущей версией будет выполнен метод onUpgrade();
    private static final int DB_VERSION = 1;

    //ссылки на DAO соответсвующие сущностям, хранимым в БД
    private MovieTableDAO movieTableDao = null;
    private MovieSerialTableDAO movieSerialTableDAO = null;
    private MovieSeasonsTableDAO movieSeasonsTableDAO = null;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    //Выполняется, когда файл с БД не найден на устройстве
    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Movie.class);
            TableUtils.createTable(connectionSource, MovieSerial.class);
            TableUtils.createTable(connectionSource, MovieSeason.class);
        } catch (SQLException e) {
            Log.e(TAG, "error creating DB " + DB_NAME);
            throw new RuntimeException(e);
        }
    }

    //Выполняется, когда БД имеет версию отличную от текущей
    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            // TODO предпочтительнее не удаляя БД аккуратно вносить изменения
            TableUtils.dropTable(connectionSource, Movie.class, true);
            TableUtils.dropTable(connectionSource, MovieSerial.class, true);
            TableUtils.dropTable(connectionSource, MovieSeason.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            Log.e(TAG, "error upgrading db " + DB_NAME + "from ver " + oldVersion);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        movieTableDao = null;
        movieSerialTableDAO = null;
        super.close();
    }

    //синглтон для MovieTableDAO
    public MovieTableDAO getMovieTableDAO() throws SQLException {
        if (movieTableDao == null) {
            movieTableDao = new MovieTableDAO(getConnectionSource(), Movie.class);
        }
        return movieTableDao;
    }

    public MovieSerialTableDAO getMovieSerialTableDAO() throws SQLException {
        if (movieSerialTableDAO == null) {
            movieSerialTableDAO = new MovieSerialTableDAO(getConnectionSource(), MovieSerial.class);
        }
        return movieSerialTableDAO;
    }

    public MovieSeasonsTableDAO getMovieSeasonsTableDAO() throws SQLException {
        if (movieSeasonsTableDAO == null) {
            movieSeasonsTableDAO = new MovieSeasonsTableDAO(getConnectionSource(), MovieSeason.class);
        }
        return movieSeasonsTableDAO;
    }
}
