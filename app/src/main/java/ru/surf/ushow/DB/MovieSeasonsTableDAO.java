package ru.surf.ushow.DB;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import ru.surf.ushow.model.MovieSeason;

public class MovieSeasonsTableDAO extends BaseDaoImpl<MovieSeason, Integer> {
    protected MovieSeasonsTableDAO(ConnectionSource connectionSource, Class<MovieSeason> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<MovieSeason> getMovieSeasonsByParentId(int parentId) throws SQLException {
        QueryBuilder<MovieSeason, Integer> queryBuilder = queryBuilder();
        queryBuilder
                .where()
                .eq(MovieSeason.NAME_FIELD_PARENT_ID, parentId);
        PreparedQuery<MovieSeason> preparedQuery = queryBuilder.prepare();
        return query(preparedQuery);
    }
}
