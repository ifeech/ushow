package ru.surf.ushow.DB;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import ru.surf.ushow.model.MovieSerial;

public class MovieSerialTableDAO extends BaseDaoImpl<MovieSerial, Integer> {

    protected MovieSerialTableDAO(ConnectionSource connectionSource, Class<MovieSerial> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public MovieSerial getMovieSerialByParentId(int parentId) throws SQLException {
        QueryBuilder<MovieSerial, Integer> queryBuilder = queryBuilder();
        queryBuilder
                .where()
                .eq(MovieSerial.NAME_FIELD_PARENT_ID, parentId);
        PreparedQuery<MovieSerial> preparedQuery = queryBuilder.prepare();
        return queryForFirst(preparedQuery);
    }
}
