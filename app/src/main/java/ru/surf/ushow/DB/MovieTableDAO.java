package ru.surf.ushow.DB;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import ru.surf.ushow.model.Movie;

public class MovieTableDAO extends BaseDaoImpl<Movie, Integer> {

    protected MovieTableDAO(ConnectionSource connectionSource, Class<Movie> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public int getMovieIdByParentExId(int exId) throws SQLException {
        QueryBuilder<Movie, Integer> queryBuilder = queryBuilder();
        queryBuilder.selectColumns(Movie.NAME_FIELD_ID);
        queryBuilder
                .where()
                .eq(Movie.NAME_FIELD_EX_ID, exId);
        PreparedQuery<Movie> preparedQuery = queryBuilder.prepare();
        Movie tempMovie = queryForFirst(preparedQuery);
        if (tempMovie != null) return queryForFirst(preparedQuery).getId();
        else return 0;
    }

    public List<Movie> getMoviesByCategory(String category) throws SQLException {
        return getMoviesByCategory(category, 500);
    }

    public List<Movie> getMoviesByCategory(String category, long limit) throws SQLException {
        QueryBuilder<Movie, Integer> queryBuilder = queryBuilder();
        queryBuilder
                .where()
                .eq(Movie.NAME_FIELD_CATEGORY, category)
                .and().not()
                .eq(Movie.NAME_FIELD_ARCHIVE, true);
        queryBuilder.limit(limit);
        PreparedQuery<Movie> preparedQuery = queryBuilder.prepare();
        return query(preparedQuery);
    }

    public List<Movie> getFavoriteMoviesByCategory(String category, Long limit) throws SQLException {
        QueryBuilder<Movie, Integer> queryBuilder = queryBuilder();
        queryBuilder
                .where()
                .eq(Movie.NAME_FIELD_CATEGORY, category)
                .and()
                .eq(Movie.NAME_FIELD_FAVORITE, true)
                .and().not()
                .eq(Movie.NAME_FIELD_ARCHIVE, true);
        queryBuilder.limit(limit);
        PreparedQuery<Movie> preparedQuery = queryBuilder.prepare();
        return query(preparedQuery);
    }

    public List<Movie> getArchiveMoviesByCategory(String category, Long limit) throws SQLException {
        QueryBuilder<Movie, Integer> queryBuilder = queryBuilder();
        queryBuilder
                .where()
                .eq(Movie.NAME_FIELD_CATEGORY, category)
                .and()
                .eq(Movie.NAME_FIELD_ARCHIVE, true);
        queryBuilder.limit(limit);
        PreparedQuery<Movie> preparedQuery = queryBuilder.prepare();
        return query(preparedQuery);
    }
}
