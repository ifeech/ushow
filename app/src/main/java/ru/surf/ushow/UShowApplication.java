package ru.surf.ushow;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.surf.ushow.API.JsonDeserializerMovie;
import ru.surf.ushow.API.MovieAPIService;
import ru.surf.ushow.DB.DBHelperFactory;
import ru.surf.ushow.model.Movie;

public class UShowApplication extends Application {

    private MovieAPIService movieApiService;
    private static UShowApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Stetho.initializeWithDefaults(this);
        DBHelperFactory.setHelper(this);

        OkHttpClient okHttpClient = new OkHttpClient.Builder().addNetworkInterceptor(chain -> {
            Request original = chain.request();
            HttpUrl url = original.url().newBuilder()
                    .addQueryParameter("api_key", MovieAPIService.API_KEY)
                    .build();

            return chain.proceed(original.newBuilder().url(url).build());
        }).addNetworkInterceptor(new StethoInterceptor()).build();

        movieApiService = new Retrofit.Builder()
                .baseUrl(MovieAPIService.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson()))
                .build().create(MovieAPIService.class);
    }

    @Override
    public void onTerminate() {
        DBHelperFactory.releaseHelper();
        super.onTerminate();
    }

    public static UShowApplication getInstance() {
        return instance;
    }

    public MovieAPIService getMovieApiService() {
        return movieApiService;
    }

    private static Gson gson() {
        return new GsonBuilder()
                .registerTypeAdapter(Movie.class, new JsonDeserializerMovie())
                .create();
    }
}
