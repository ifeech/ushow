package ru.surf.ushow.loader;

import android.content.AsyncTaskLoader;
import android.content.Context;

import java.io.IOException;

import retrofit2.Call;
import ru.surf.ushow.UShowApplication;
import ru.surf.ushow.model.Token;

/**
 * Получение токена по API
 */
public class TokenAsyncTaskLoader extends AsyncTaskLoader<Token> {

    private Token token;

    public TokenAsyncTaskLoader(Context context) {
        super(context);
    }

    @Override
    protected void onStartLoading() {
        if(token != null){
            deliverResult(token);
        }
        if(token == null || takeContentChanged()){
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();
    }

    @Override
    public Token loadInBackground() {
        Call<Token> call = UShowApplication.getInstance().getMovieApiService().getToken();
        try {
            token = call.execute().body();
            return token;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
