package ru.surf.ushow.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import ru.surf.ushow.activity.EditMovieActivity;
import ru.surf.ushow.activity.MovieDetailActivity;
import ru.surf.ushow.R;
import ru.surf.ushow.model.Movie;

/**
 * Вывод фильмов в виде списка
 */
public class MoviesListAdapter extends RecyclerView.Adapter<MoviesListAdapter.MoviesViewHolder> {

    private List<Movie> movies;

    public MoviesListAdapter(List<Movie> obj) {
        this.movies = obj;
    }

    @Override
    public MoviesListAdapter.MoviesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_list_item, parent, false);
        return new MoviesViewHolder(view, parent.getContext());
    }

    @Override
    public void onBindViewHolder(final MoviesViewHolder holder, final int position) {
        final Movie movie = movies.get(position);
        if (movie != null) {
            holder.movieTitle.setText(movie.getTitle());
            holder.movieOverview.setText(movie.getOverview());
            Glide.with(holder.context).load(movie.getPoster_w780()).placeholder(R.drawable.ic_panorama_black_24dp).into(holder.moviePoster);
            holder.cardMovie.setOnClickListener(v -> MovieDetailActivity.startByApi(
                    holder.context,
                    movie.getExId(),
                    movie.getMediaType(),
                    movie.getTitle(),
                    movie.getPoster_w780()
            ));
        }
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public class MoviesViewHolder extends RecyclerView.ViewHolder {

        private Context context;
        private CardView cardMovie;
        private TextView movieTitle;
        private TextView movieOverview;
        private ImageView moviePoster;

        public MoviesViewHolder(View itemView, Context context) {
            super(itemView);
            this.context = context;
            cardMovie = (CardView) itemView.findViewById(R.id.movie_list_item_card_movie);
            movieTitle = (TextView) itemView.findViewById(R.id.movie_list_item_movie_title_label);
            movieOverview = (TextView) itemView.findViewById(R.id.movie_list_item_movie_overview_label);
            moviePoster = (ImageView) itemView.findViewById(R.id.movie_list_item_movie_poster);
        }
    }
}
