package ru.surf.ushow.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.sql.SQLException;

import ru.surf.ushow.DB.DBHelperFactory;
import ru.surf.ushow.DialogUtil;
import ru.surf.ushow.R;
import ru.surf.ushow.model.Movie;
import ru.surf.ushow.model.MovieSeason;
import ru.surf.ushow.model.MovieSerial;
import ru.surf.ushow.loader.GetMovieByIdLoader;

public class MovieDetailActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Movie> {

    private final static int LOADER_GET_MOVIES_ID = 1;
    private final static String EXTRA_MOVIE_ID = "extra_movie_id";
    private final static String EXTRA_MOVIE_EX_ID = "extra_movie_ex_id";
    private final static String EXTRA_MOVIE_TITLE = "extra_movie_title";
    private final static String EXTRA_MOVIE_POSTER = "extra_movie_poster";
    private final static String EXTRA_MOVIE_MEDIA_TYPE = "extra_movie_media_type";
    private final static int PLACE_DATA_DB = 1;
    private final static int PLACE_DATA_API = 2;
    private final static int SAVE_MOVIE_BTN_STATUS_ADD = 1;
    private final static int SAVE_MOVIE_BTN_STATUS_REMOVE = 2;

    private final static String EXTRA_MOVIE_PLACE_DATA = "extra_movie_place_data";

    private AlertDialog.Builder removeAlertDialog;
    private ProgressBar loadProgressBar;
    private TextView movieSeasonStopLabel, movieCurrentSeasonLabel;
    private TextView movieEpisodeStopLabel, movieCurrentEpisodeLabel;
    private LinearLayout movieSerialContainer;
    private LinearLayout movieArchiveContainer;
    private CollapsingToolbarLayout collapsingToolbar;
    private ImageView poster;
    private SeekBar currentSeason, currentEpisode;
    private FloatingActionButton saveMovieBtn;
    private TextView viewedMovieBtn;
    private Movie movie;
    private MovieSerial movieSerial;
    private int moviePlaceData;
    private int saveMovieBtnStatus;

    private Bundle bundle;

    public static void startByApi(Context context, int exId, String movieType, String title, String poster) {
        Intent i = new Intent(context, MovieDetailActivity.class);
        i.putExtra(EXTRA_MOVIE_EX_ID, exId);
        i.putExtra(EXTRA_MOVIE_MEDIA_TYPE, movieType);
        i.putExtra(EXTRA_MOVIE_TITLE, title);
        i.putExtra(EXTRA_MOVIE_POSTER, poster);
        i.putExtra(EXTRA_MOVIE_PLACE_DATA, PLACE_DATA_API);
        context.startActivity(i);
    }

    public static void startByDB(Context context, int id, String title, String poster) {
        Intent i = new Intent(context, MovieDetailActivity.class);
        i.putExtra(EXTRA_MOVIE_ID, id);
        i.putExtra(EXTRA_MOVIE_TITLE, title);
        i.putExtra(EXTRA_MOVIE_POSTER, poster);
        i.putExtra(EXTRA_MOVIE_PLACE_DATA, PLACE_DATA_DB);
        context.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_detail_activity);

        // устанавливаем toolbar как actionbar (для совместимости)
        setSupportActionBar((Toolbar) findViewById(R.id.movie_detail_activity_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        poster = (ImageView) findViewById(R.id.movie_detail_activity_poster);
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.movie_detail_activity_collapsing_toolbar);
        movieSerialContainer = (LinearLayout) findViewById(R.id.movie_detail_activity_serial_container);
        movieSeasonStopLabel = (TextView) findViewById(R.id.movie_detail_activity_current_season_stop_label);
        movieCurrentSeasonLabel = (TextView) findViewById(R.id.movie_detail_activity_current_season_label);
        movieEpisodeStopLabel = (TextView) findViewById(R.id.movie_detail_activity_current_episode_stop_label);
        movieCurrentEpisodeLabel = (TextView) findViewById(R.id.movie_detail_activity_current_episode_label);
        loadProgressBar = (ProgressBar) findViewById(R.id.movie_detail_activity_load_progress_bar);
        movieArchiveContainer = (LinearLayout) findViewById(R.id.movie_detail_activity_movie_archive_container);

        Intent intent = getIntent();
        int movieExId = intent.getIntExtra(EXTRA_MOVIE_EX_ID, 0);
        int movieId = intent.getIntExtra(EXTRA_MOVIE_ID, 0);
        moviePlaceData = intent.getIntExtra(EXTRA_MOVIE_PLACE_DATA, 0);
        collapsingToolbar.setTitle(intent.getStringExtra(EXTRA_MOVIE_TITLE));
        Glide.with(this).load(intent.getStringExtra(EXTRA_MOVIE_POSTER)).into(poster);

        // Получение данных из api или бд
        if (movieExId != 0) {
            // Проверка на существования movie в бд
            try {
                movieId = DBHelperFactory.getHelper().getMovieTableDAO().getMovieIdByParentExId(movieExId);
                if (movieId != 0) {
                    moviePlaceData = PLACE_DATA_DB;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        switch (moviePlaceData) {
            case PLACE_DATA_API:
                saveMovieBtnStatus = SAVE_MOVIE_BTN_STATUS_ADD;
                bundle = GetMovieByIdLoader.createBundleByAPI(movieExId, moviePlaceData, intent.getStringExtra(EXTRA_MOVIE_MEDIA_TYPE));
                break;
            case PLACE_DATA_DB:
                saveMovieBtnStatus = SAVE_MOVIE_BTN_STATUS_REMOVE;
                bundle = GetMovieByIdLoader.createBundleByDB(movieId, moviePlaceData);
                break;
            default:
                break;
        }
        getSupportLoaderManager().restartLoader(LOADER_GET_MOVIES_ID, bundle, this);

        currentEpisode = (SeekBar) findViewById(R.id.movie_detail_activity_current_episode);
        currentEpisode.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String strFormat = String.format(
                        "%s %d",
                        getString(R.string.movie_detail_activity_current_episode_label_text),
                        progress + 1
                );
                movieCurrentEpisodeLabel.setText(strFormat);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (moviePlaceData == PLACE_DATA_DB) {
                    updateEpisode(seekBar.getProgress());
                }
            }
        });

        currentSeason = (SeekBar) findViewById(R.id.movie_detail_activity_current_season);
        currentSeason.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String strFormat = String.format("%s %d",
                        getString(R.string.movie_detail_activity_current_season_label_text),
                        progress + 1);
                movieCurrentSeasonLabel.setText(strFormat);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                currentEpisode.setMax(getEpisodeCountBySeason() - 1);
                movieEpisodeStopLabel.setText(String.valueOf(getEpisodeCountBySeason()));

                if (moviePlaceData == PLACE_DATA_DB) {
                    updateSeason(seekBar.getProgress());
                }
            }
        });

        // добавление movie
        saveMovieBtn = (FloatingActionButton) findViewById(R.id.movie_detail_activity_add_movie_btn);
        if (saveMovieBtnStatus == SAVE_MOVIE_BTN_STATUS_ADD) {
            saveMovieBtn.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_add_white_24dp));
        } else {
            saveMovieBtn.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_check_white_24dp));
        }
        saveMovieBtn.setOnClickListener(v -> {
            switch (saveMovieBtnStatus) {
                case SAVE_MOVIE_BTN_STATUS_ADD:
                    //Добавление movie из API
                    if (moviePlaceData == PLACE_DATA_API) {
                        try {
                            DBHelperFactory.getHelper().getMovieTableDAO().create(movie);
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                        // Добавление серий
                        if (movie.isSerial() && movieSerial != null) {
                            movieSerial.setParentId(movie.getId());
                            movieSerial.setCurrentSeason(currentSeason.getProgress() + 1);
                            movieSerial.setCurrentEpisode(currentEpisode.getProgress() + 1);
                            try {
                                DBHelperFactory.getHelper().getMovieSerialTableDAO().create(movieSerial);
                                for (MovieSeason movieSeason : movieSerial.getListSeasons()) {
                                    movieSeason.setParentId(movieSerial.getId());
                                    DBHelperFactory.getHelper().getMovieSeasonsTableDAO().create(movieSeason);
                                }
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        }

                        Toast.makeText(getApplicationContext(),
                                getString(R.string.movie_detail_activity_save_movie_toast_text),
                                Toast.LENGTH_SHORT
                        ).show();

                        moviePlaceData = PLACE_DATA_DB;
                        invalidateOptionsMenu();
                        saveMovieBtnStatus = SAVE_MOVIE_BTN_STATUS_REMOVE;
                        ((FloatingActionButton) v).setImageDrawable(ContextCompat.getDrawable(MovieDetailActivity.this, R.drawable.ic_check_white_24dp));
                    }
                    break;
                case SAVE_MOVIE_BTN_STATUS_REMOVE:
                    showRemoveDialog();
                    break;
            }

        });

        // Просмотр movie, серии movie
        viewedMovieBtn = (TextView) findViewById(R.id.movie_detail_activity_viewed_movie_btn);
        viewedMovieBtn.setOnClickListener(v -> {
            if (movie.isSerial()) {
                updateEpisode(currentEpisode.getProgress() + 1);
            } else {
                movie.setArchive(true);
                try {
                    DBHelperFactory.getHelper().getMovieTableDAO().update(movie);
                    v.setVisibility(View.GONE);
                    movieArchiveContainer.setVisibility(View.VISIBLE);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        movieArchiveContainer.setOnClickListener(v -> {
            movie.setArchive(false);
            try {
                DBHelperFactory.getHelper().getMovieTableDAO().update(movie);
                v.setVisibility(View.GONE);
                viewedMovieBtn.setVisibility(View.VISIBLE);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    // Окно предупреждения об удалении
    private void showRemoveDialog() {
        if (removeAlertDialog == null) {
            removeAlertDialog = DialogUtil.createRemoveMovieDialog(this, (dialog, which) -> {
                try {
                    DBHelperFactory.getHelper().getMovieTableDAO().deleteById(movie.getId());
                    if (movieSerial != null) {
                        DBHelperFactory.getHelper().getMovieSeasonsTableDAO().delete(movieSerial.getListSeasons());
                        DBHelperFactory.getHelper().getMovieSerialTableDAO().deleteById(movieSerial.getId());
                    }

                    Toast.makeText(getApplicationContext(),
                            getString(R.string.movie_detail_activity_delete_movie_toast_text),
                            Toast.LENGTH_SHORT
                    ).show();

                    MainActivity.start(MovieDetailActivity.this);
                    finish();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        }
        removeAlertDialog.show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getSupportLoaderManager().restartLoader(LOADER_GET_MOVIES_ID, bundle, this);
    }

    @Override
    public Loader<Movie> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LOADER_GET_MOVIES_ID:
                return new GetMovieByIdLoader(this, args);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Movie> loader, Movie data) {
        loadProgressBar.setVisibility(View.GONE);
        switch (loader.getId()) {
            case LOADER_GET_MOVIES_ID:
                if (data != null) {
                    TextView overviewLabel = (TextView) findViewById(R.id.movie_detail_activity_movie_overview_label);
                    TextView runtimeLabel = (TextView) findViewById(R.id.movie_detail_activity_runtime_label);
                    TextView statusLabel = (TextView) findViewById(R.id.movie_detail_activity_status_label);
                    TextView airingLabel = (TextView) findViewById(R.id.movie_detail_activity_airing_label);
                    TextView descriptionLabel = (TextView) findViewById(R.id.movie_detail_activity_description_label);
                    TextView emptyContentLabel = (TextView) findViewById(R.id.movie_detail_activity_empty_content_label);
                    LinearLayout statusContainer = (LinearLayout) findViewById(R.id.movie_detail_activity_status_container);
                    LinearLayout contentContainer = (LinearLayout) findViewById(R.id.movie_detail_activity_content_container);
                    String strFormat;
                    movie = data;

                    collapsingToolbar.setTitle(movie.getTitle());
                    Glide.with(this).load(movie.getPoster_w780()).into(poster);
                    if (!TextUtils.isEmpty(data.getOverview())) {
                        overviewLabel.setText(data.getOverview());
                        overviewLabel.setVisibility(View.VISIBLE);
                    } else {
                        overviewLabel.setVisibility(View.GONE);
                    }
                    if (data.getRuntime() != 0) {
                        strFormat = String.format("%s %d %s",
                                getString(R.string.movie_detail_activity_runtime_label),
                                data.getRuntime(),
                                getString(R.string.movie_detail_activity_min_label));
                        runtimeLabel.setText(strFormat);
                        runtimeLabel.setVisibility(View.VISIBLE);
                    } else {
                        runtimeLabel.setVisibility(View.GONE);
                    }
                    if (!TextUtils.isEmpty(data.getStatus()) || !TextUtils.isEmpty(data.getAiringYear())) {
                        strFormat = String.format("%s %s",
                                getString(R.string.movie_detail_activity_status_label),
                                data.getStatus());
                        statusLabel.setText(strFormat);

                        strFormat = String.format("%s %s",
                                getString(R.string.movie_detail_activity_airing_label),
                                data.getAiringYear());
                        airingLabel.setText(strFormat);
                        statusContainer.setVisibility(View.VISIBLE);
                    } else {
                        statusContainer.setVisibility(View.GONE);
                    }
                    if (data.hasDescription()) {
                        descriptionLabel.setVisibility(View.VISIBLE);
                    } else {
                        descriptionLabel.setVisibility(View.GONE);
                    }

                    movieSerial = movie.getMovieSerial();
                    if (data.isSerial() && movieSerial != null) {
                        if (movieSerial.getCurrentSeason() == 0) {
                            movieSerial.setCurrentSeason(1);
                        }
                        if (movieSerial.getCurrentEpisode() == 0) {
                            movieSerial.setCurrentEpisode(1);
                        }
                        currentSeason.setProgress(movieSerial.getCurrentSeason() - 1);
                        currentEpisode.setProgress(movieSerial.getCurrentEpisode() - 1);
                        strFormat = String.format("%s %d",
                                getString(R.string.movie_detail_activity_current_season_label_text),
                                currentSeason.getProgress() + 1);
                        movieCurrentSeasonLabel.setText(strFormat);
                        strFormat = String.format("%s %d",
                                getString(R.string.movie_detail_activity_current_episode_label_text),
                                currentEpisode.getProgress() + 1);
                        movieCurrentEpisodeLabel.setText(strFormat);

                        currentSeason.setMax(getMaxSeasons() - 1);
                        movieSeasonStopLabel.setText(String.valueOf(getMaxSeasons()));
                        currentEpisode.setMax(getEpisodeCountBySeason() - 1);
                        movieEpisodeStopLabel.setText(String.valueOf(getEpisodeCountBySeason()));
                        movieSerialContainer.setVisibility(View.VISIBLE);
                    } else {
                        movieSerialContainer.setVisibility(View.GONE);
                    }

                    // Проверка на последний эпизод последнего сезона
                    if (moviePlaceData == PLACE_DATA_DB) {
                        if (!movie.isArchive()) {
                            viewedMovieBtn.setVisibility(View.VISIBLE);
                        } else {
                            movieArchiveContainer.setVisibility(View.VISIBLE);
                        }
                    }

                    // загрузка данных завершена
                    saveMovieBtn.setVisibility(View.VISIBLE);
                    contentContainer.setVisibility(View.VISIBLE);
                    if (data.hasDescription()) {
                        emptyContentLabel.setVisibility(View.GONE);
                    } else {
                        emptyContentLabel.setVisibility(View.VISIBLE);
                    }
                }
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Movie> loader) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.movie_detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.movie_detail_menu_action_favorite:
                movie.setFavorite(!movie.isFavorite());
                try {
                    DBHelperFactory.getHelper().getMovieTableDAO().update(movie);
                    if (movie.isFavorite()) {
                        item.setIcon(R.drawable.ic_favorite_active);
                    } else {
                        item.setIcon(R.drawable.ic_favorite_border_white_no_active);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                return true;
            case R.id.movie_detail_menu_action_edit:
                EditMovieActivity.startByDB(MovieDetailActivity.this, movie.getId());
                return true;
            case R.id.movie_detail_menu_action_remove:
                showRemoveDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (moviePlaceData == PLACE_DATA_API) {
            menu.findItem(R.id.movie_detail_menu_action_favorite).setVisible(false);
            menu.findItem(R.id.movie_detail_menu_action_edit).setVisible(false);
            menu.findItem(R.id.movie_detail_menu_action_remove).setVisible(false);
        } else {
            menu.findItem(R.id.movie_detail_menu_action_favorite).setVisible(true);
            menu.findItem(R.id.movie_detail_menu_action_edit).setVisible(true);
            menu.findItem(R.id.movie_detail_menu_action_remove).setVisible(true);

            if (movie != null && movie.isFavorite()) {
                menu.findItem(R.id.movie_detail_menu_action_favorite).setIcon(R.drawable.ic_favorite_active);
            } else {
                menu.findItem(R.id.movie_detail_menu_action_favorite).setIcon(R.drawable.ic_favorite_border_white_no_active);
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    private int getMaxSeasons() {
        if (movieSerial != null) {
            return movieSerial.getNumberSeasons();
        } else {
            return MovieSerial.MAX_NUMBER_SEASONS;
        }
    }

    // количество эпизодов в сезоне
    private int getEpisodeCountBySeason() {
        MovieSeason movieSeason = null;
        if (movieSerial != null) {
            movieSeason = movieSerial.getListSeasons().get(currentSeason.getProgress());
        }
        if (movieSeason != null) {
            return movieSeason.getEpisodeCount();
        } else {
            return MovieSerial.MAX_NUMBER_EPISODES;
        }
    }

    private void updateSeason(int numberSeason) {
        numberSeason++;
        movieSerial.setCurrentSeason(numberSeason);
        updateEpisode(currentEpisode.getProgress());
        try {
            DBHelperFactory.getHelper().getMovieSerialTableDAO().update(movieSerial);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void updateEpisode(int numberEpisode) {
        numberEpisode++;
        try {
            if (numberEpisode > getEpisodeCountBySeason()) {
                if (getMaxSeasons() == movieSerial.getCurrentSeason()) {
                    movie.setArchive(true);
                    DBHelperFactory.getHelper().getMovieTableDAO().update(movie);

                    Toast.makeText(getApplicationContext(),
                            getString(R.string.movie_detail_activity_last_episode_toast_text),
                            Toast.LENGTH_SHORT).show();
                    viewedMovieBtn.setVisibility(View.GONE);
                    movieArchiveContainer.setVisibility(View.VISIBLE);

                } else {
                    if (movie.isArchive()) {
                        movie.setArchive(false);
                        viewedMovieBtn.setVisibility(View.VISIBLE);
                        movieArchiveContainer.setVisibility(View.GONE);
                        DBHelperFactory.getHelper().getMovieTableDAO().update(movie);
                    }
                    movieSerial.setCurrentSeason(movieSerial.getCurrentSeason() + 1);
                    movieSerial.setCurrentEpisode(1);
                }
            } else {
                if (movie.isArchive()) {
                    movie.setArchive(false);
                    viewedMovieBtn.setVisibility(View.VISIBLE);
                    movieArchiveContainer.setVisibility(View.GONE);
                    DBHelperFactory.getHelper().getMovieTableDAO().update(movie);
                }
                movieSerial.setCurrentEpisode(numberEpisode);
            }

            DBHelperFactory.getHelper().getMovieSerialTableDAO().update(movieSerial);
            currentSeason.setProgress(movieSerial.getCurrentSeason() - 1);
            currentEpisode.setProgress(movieSerial.getCurrentEpisode() - 1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
