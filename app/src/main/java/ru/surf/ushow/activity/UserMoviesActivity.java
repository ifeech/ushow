package ru.surf.ushow.activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import ru.surf.ushow.R;
import ru.surf.ushow.adapter.UserMoviesFragmentPagerAdapter;
import ru.surf.ushow.fragments.AUserMoviesFragment;
import ru.surf.ushow.model.Movie;

public class UserMoviesActivity extends AppCompatActivity {

    public static void start(Context context, String category) {
        Intent i = new Intent(context, UserMoviesActivity.class);
        i.putExtra(AUserMoviesFragment.EXTRA_MOVIE_CATEGORY, category);
        context.startActivity(i);
    }

    public static void startFilms(Context context) {
        start(context, Movie.NAME_CATEGORY_FILM);
    }

    public static void startTV(Context context) {
        start(context, Movie.NAME_CATEGORY_TV);
    }

    public static void startCartoons(Context context) {
        start(context, Movie.NAME_CATEGORY_CARTOON);
    }

    public static void startAnime(Context context) {
        start(context, Movie.NAME_CATEGORY_ANIME);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_movies_activity);

        // устанавливаем toolbar как actionbar (для совместимости)
        setSupportActionBar((Toolbar) findViewById(R.id.user_movies_activity_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.user_movies_activity_title));

        ViewPager userMoviesFragmentsPager = (ViewPager) findViewById(R.id.user_movies_activity_fragment_pager);
        userMoviesFragmentsPager.setAdapter(new UserMoviesFragmentPagerAdapter(this, getSupportFragmentManager()));
        TabLayout tabLayout = (TabLayout) findViewById(R.id.user_movies_activity_tab_layout);
        tabLayout.setupWithViewPager(userMoviesFragmentsPager);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
