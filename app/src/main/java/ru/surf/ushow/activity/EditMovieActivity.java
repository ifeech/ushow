package ru.surf.ushow.activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.jakewharton.rxbinding.view.RxView;
import com.jakewharton.rxbinding.widget.RxAdapterView;
import com.jakewharton.rxbinding.widget.RxCompoundButton;
import com.jakewharton.rxbinding.widget.RxSeekBar;
import com.jakewharton.rxbinding.widget.RxTextView;
import com.jakewharton.rxbinding.widget.SeekBarStopChangeEvent;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ru.surf.ushow.DB.DBHelperFactory;
import ru.surf.ushow.R;
import ru.surf.ushow.loader.GetMovieByIdLoader;
import ru.surf.ushow.model.Movie;
import ru.surf.ushow.model.MovieSeason;
import ru.surf.ushow.model.MovieSerial;
import rx.functions.Action1;
import rx.functions.Func1;

// TODO сохранять состояние выбора при повороте
public class EditMovieActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Movie> {

    private final static int LOADER_GET_MOVIES_ID = 1;
    private final static String EXTRA_MOVIE_ID = "extra_movie_id";
    private final static String EXTRA_MOVIE_TITLE = "extra_movie_title";
    private final static String EXTRA_MOVIE_PLACE_DATA = "extra_movie_place_data";
    private final static int PLACE_DATA_DB = 1;
    private final static int PLACE_DATA_CREATE = 3;

    private LinearLayout serialContainer;
    private EditText episodeStopText;
    private EditText seasonStopText;
    private TextView seasonNumberLabel;
    private TextView episodeCountLabel;
    private SeekBar episodeCount;
    private SeekBar seasonNumber;
    private CheckBox isSerialCheckBox;
    private ProgressBar loadProgressBar;
    private TextInputEditText titleText;
    private TextInputEditText overviewText;
    private TextInputEditText runtimeText;
    private ImageView poster;
    private Spinner mediaTypeSpinner;
    private int moviePlaceData;
    private int currentSeasonNumber, maxSeasonNumber;
    private Movie movie = new Movie();
    private MovieSerial movieSerial = new MovieSerial();
    private List<MovieSeason> movieSeasons = new ArrayList<>();

    public static void startByDB(Context context, int id) {
        Intent i = new Intent(context, EditMovieActivity.class);
        i.putExtra(EXTRA_MOVIE_ID, id);
        i.putExtra(EXTRA_MOVIE_PLACE_DATA, PLACE_DATA_DB);
        context.startActivity(i);
    }

    public static void start(Context context, String title) {
        Intent i = new Intent(context, EditMovieActivity.class);
        i.putExtra(EXTRA_MOVIE_TITLE, title);
        i.putExtra(EXTRA_MOVIE_PLACE_DATA, PLACE_DATA_CREATE);
        context.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_movie_activity);

        // устанавливаем toolbar как actionbar (для совместимости)
        setSupportActionBar((Toolbar) findViewById(R.id.edit_movie_activity_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        serialContainer = (LinearLayout) findViewById(R.id.edit_movie_activity_serial_container);
        titleText = (TextInputEditText) findViewById(R.id.edit_movie_activity_title_text);
        overviewText = (TextInputEditText) findViewById(R.id.edit_movie_activity_overview_text);
        runtimeText = (TextInputEditText) findViewById(R.id.edit_movie_activity_runtime_text);
        poster = (ImageView) findViewById(R.id.edit_movie_activity_poster);
        loadProgressBar = (ProgressBar) findViewById(R.id.edit_movie_activity_load_data_progress_bar);
        seasonStopText = (EditText) findViewById(R.id.edit_movie_activity_season_number_stop_text);
        seasonNumberLabel = (TextView) findViewById(R.id.edit_movie_activity_season_number_label);
        episodeStopText = (EditText) findViewById(R.id.edit_movie_activity_episode_count_stop_text);
        episodeCountLabel = (TextView) findViewById(R.id.edit_movie_activity_episode_count_label);

        Intent intent = getIntent();
        moviePlaceData = intent.getIntExtra(EXTRA_MOVIE_PLACE_DATA, 0);
        final int movieId = intent.getIntExtra(EXTRA_MOVIE_ID, 0);
        String movieTitle = intent.getStringExtra(EXTRA_MOVIE_TITLE);

        switch (moviePlaceData) {
            case PLACE_DATA_DB:
                Bundle bundle = GetMovieByIdLoader.createBundleByDB(movieId, moviePlaceData);
                getSupportLoaderManager().restartLoader(LOADER_GET_MOVIES_ID, bundle, this);
                break;
            case PLACE_DATA_CREATE:
                getSupportActionBar().setTitle(getString(R.string.edit_movie_activity_title_create_text));
                titleText.setText(movieTitle);
                Glide.with(EditMovieActivity.this).load(Movie.URL_POSTER_DEFAULT).into(poster);
                findViewById(R.id.edit_movie_activity_scroll_content).setVisibility(View.VISIBLE);
                findViewById(R.id.edit_movie_activity_save_btn).setVisibility(View.VISIBLE);
                loadProgressBar.setVisibility(View.GONE);
                break;
            default:
                break;
        }

        episodeCount = (SeekBar) findViewById(R.id.edit_movie_activity_episode_count);
        RxSeekBar.changeEvents(episodeCount)
                .filter(integer -> !movieSeasons.isEmpty() && currentSeasonNumber != 0)
                .subscribe(seekBarChangeEvent -> {
                    String strFormat = String.format("%s %d",
                            getString(R.string.edit_movie_activity_episode_count_label_text),
                            episodeCount.getProgress() + 1);
                    episodeCountLabel.setText(strFormat);
                    if (seekBarChangeEvent instanceof SeekBarStopChangeEvent) {
                        movieSeasons.get(currentSeasonNumber - 1).setEpisodeCount(episodeCount.getProgress() + 1);
                    }
                });

        seasonNumber = (SeekBar) findViewById(R.id.edit_movie_activity_season_number);
        RxSeekBar.changeEvents(seasonNumber).subscribe(seekBarChangeEvent -> {
            String strFormat = String.format("%s %d",
                    getString(R.string.edit_movie_activity_season_number_label_text),
                    seasonNumber.getProgress() + 1);
            seasonNumberLabel.setText(strFormat);
            if (seekBarChangeEvent instanceof SeekBarStopChangeEvent) {
                currentSeasonNumber = seasonNumber.getProgress() + 1;
                episodeCount.setMax(getEpisodeCountBySeason() - 1);
                episodeCount.setProgress(getEpisodeCountBySeason() - 1);
                episodeStopText.setText(String.valueOf(getEpisodeCountBySeason()));
            }
        });

        isSerialCheckBox = (CheckBox) findViewById(R.id.edit_movie_activity_is_serial_checkbox);
        isSerialCheckBox.setChecked(false);
        isSerialCheckBox.setEnabled(false);
        RxCompoundButton.checkedChanges(isSerialCheckBox).subscribe(aBoolean -> {
            if (aBoolean) {
                if (movieSeasons.isEmpty()) {
                    currentSeasonNumber = 1;
                    maxSeasonNumber = 1;
                    movieSeasons.add(new MovieSeason(getEpisodeCountBySeason(), currentSeasonNumber));

                    seasonNumber.setMax(maxSeasonNumber - 1);
                    seasonStopText.setText(String.valueOf(maxSeasonNumber));
                    episodeCount.setMax(getEpisodeCountBySeason() - 1);
                    episodeStopText.setText(String.valueOf(getEpisodeCountBySeason()));
                }
                serialContainer.setVisibility(View.VISIBLE);
            } else {
                serialContainer.setVisibility(View.GONE);
            }
        });

        // список категорий movie
        ArrayAdapter movieCategoriesAdapter = ArrayAdapter.createFromResource(this, R.array.movie_categories, android.R.layout.simple_spinner_item);
        movieCategoriesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mediaTypeSpinner = (Spinner) findViewById(R.id.edit_movie_activity_media_type_spinner);
        mediaTypeSpinner.setAdapter(movieCategoriesAdapter);
        RxAdapterView.itemSelections(mediaTypeSpinner).subscribe(position -> {
            switch (position) {
                case 0:
                    isSerialCheckBox.setChecked(false);
                    isSerialCheckBox.setEnabled(false);
                    break;
                case 1:
                    isSerialCheckBox.setChecked(true);
                    isSerialCheckBox.setEnabled(false);
                    break;
                default:
                    isSerialCheckBox.setEnabled(true);
                    break;
            }
        });

        RxTextView.afterTextChangeEvents(seasonStopText)
                .filter(textViewAfterTextChangeEvent -> !movieSeasons.isEmpty())
                .filter(textViewAfterTextChangeEvent -> textViewAfterTextChangeEvent.editable().length() != 0)
                .filter(textViewAfterTextChangeEvent -> Integer.parseInt(textViewAfterTextChangeEvent.editable().toString()) != maxSeasonNumber)
                .subscribe(textViewAfterTextChangeEvent -> {
                    maxSeasonNumber = Integer.parseInt(seasonStopText.getText().toString());
                    maxSeasonNumber = (maxSeasonNumber < 1) ? 1 : (maxSeasonNumber > MovieSerial.MAX_NUMBER_SEASONS) ? MovieSerial.MAX_NUMBER_SEASONS : maxSeasonNumber;
                    if (maxSeasonNumber > movieSeasons.size()) {
                        for (int i = movieSeasons.size(); i < maxSeasonNumber; i++) {
                            movieSeasons.add(new MovieSeason(getEpisodeCountBySeason(), i + 1));
                        }
                    } else if (maxSeasonNumber < movieSeasons.size()) {
                        for (int i = movieSeasons.size() - 1; i >= maxSeasonNumber; i--) {
                            movieSeasons.remove(i);
                        }
                    }
                    currentSeasonNumber = maxSeasonNumber;
                    seasonNumber.setMax(maxSeasonNumber - 1);
                    seasonNumber.setProgress(maxSeasonNumber - 1);
                    textViewAfterTextChangeEvent.editable().clear();
                    textViewAfterTextChangeEvent.editable().append(String.valueOf(maxSeasonNumber));
                    String strFormat = String.format("%s %d",
                            getString(R.string.edit_movie_activity_season_number_label_text),
                            maxSeasonNumber);
                    seasonNumberLabel.setText(strFormat);
                });
        RxView.focusChanges(seasonStopText)
                .filter(aBoolean -> !aBoolean && seasonStopText.length() == 0)
                .subscribe(aBoolean -> {
                    seasonStopText.setText(String.valueOf(maxSeasonNumber));
                });

        RxTextView.afterTextChangeEvents(episodeStopText)
                .filter(textViewAfterTextChangeEvent -> !movieSeasons.isEmpty())
                .filter(textViewAfterTextChangeEvent -> textViewAfterTextChangeEvent.editable().length() != 0)
                .filter(textViewAfterTextChangeEvent -> Integer.parseInt(textViewAfterTextChangeEvent.editable().toString()) != getEpisodeCountBySeason())
                .subscribe(textViewAfterTextChangeEvent -> {
                    int currentEpisodeCount = Integer.parseInt(textViewAfterTextChangeEvent.editable().toString());
                    currentEpisodeCount = (currentEpisodeCount < 1) ? 1 : (currentEpisodeCount > MovieSerial.MAX_NUMBER_EPISODES) ? MovieSerial.MAX_NUMBER_EPISODES : currentEpisodeCount;
                    movieSeasons.get(currentSeasonNumber - 1).setEpisodeCount(currentEpisodeCount);
                    episodeCount.setMax(currentEpisodeCount - 1);
                    episodeCount.setProgress(currentEpisodeCount - 1);
                    textViewAfterTextChangeEvent.editable().clear();
                    textViewAfterTextChangeEvent.editable().append(String.valueOf(currentEpisodeCount));
                    String strFormat = String.format("%s %d",
                            getString(R.string.edit_movie_activity_episode_count_label_text),
                            currentEpisodeCount);
                    episodeCountLabel.setText(strFormat);
                });
        RxView.focusChanges(episodeStopText)
                .filter(aBoolean -> !aBoolean && episodeStopText.length() == 0)
                .subscribe(aBoolean -> {
                    episodeStopText.setText(String.valueOf(getEpisodeCountBySeason()));
                });

        // добавление movie в БД
        Button saveMovieBtn = (Button) findViewById(R.id.edit_movie_activity_save_btn);
        RxTextView.textChanges(titleText)
                .subscribe(charSequence -> {
                    if (charSequence.length() > 0) {
                        saveMovieBtn.setEnabled(true);
                    } else {
                        saveMovieBtn.setEnabled(false);
                    }
                });
        RxView.clicks(saveMovieBtn).subscribe(aVoid -> {
            //Добавление фильма
            movie.setTitle(titleText.getText().toString());
            movie.setOverview(overviewText.getText().toString());
            if (!runtimeText.getText().toString().equals("")) {
                movie.setRuntime(Integer.parseInt(runtimeText.getText().toString()));
            } else {
                movie.setRuntime(0);
            }
            // TODO poster (default не сохранять)

            switch (mediaTypeSpinner.getSelectedItemPosition()) {
                case 0:
                    movie.setCategory(Movie.NAME_CATEGORY_FILM);
                    isSerialCheckBox.setChecked(false);
                    break;
                case 1:
                    movie.setCategory(Movie.NAME_CATEGORY_TV);
                    isSerialCheckBox.setChecked(true);
                    break;
                case 2:
                    movie.setCategory(Movie.NAME_CATEGORY_CARTOON);
                    break;
                case 3:
                    movie.setCategory(Movie.NAME_CATEGORY_ANIME);
                    break;
                default:
                    movie.setCategory(Movie.NAME_CATEGORY_FILM);
                    break;
            }

            if (moviePlaceData == PLACE_DATA_CREATE) {
                try {
                    movie.setSerial(isSerialCheckBox.isChecked());
                    DBHelperFactory.getHelper().getMovieTableDAO().create(movie);

                    // Добавление серий
                    if (movie.isSerial()) {
                        movieSerial = new MovieSerial();
                        movieSerial.setParentId(movie.getId());
                        movieSerial.setCurrentSeason(1);
                        movieSerial.setCurrentEpisode(1);
                        movieSerial.setNumberSeasons(maxSeasonNumber);

                        DBHelperFactory.getHelper().getMovieSerialTableDAO().create(movieSerial);
                        for (MovieSeason movieSeason : movieSeasons) {
                            movieSeason.setParentId(movieSerial.getId());
                            DBHelperFactory.getHelper().getMovieSeasonsTableDAO().create(movieSeason);
                        }
                    }
                    Toast.makeText(getApplicationContext(),
                            getString(R.string.edit_movie_activity_save_movie_toast_text),
                            Toast.LENGTH_SHORT).show();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    if (movie.isSerial() && !isSerialCheckBox.isChecked() && movieSerial != null) {
                        DBHelperFactory.getHelper().getMovieSeasonsTableDAO().delete(movieSerial.getListSeasons());
                        DBHelperFactory.getHelper().getMovieSerialTableDAO().deleteById(movieSerial.getId());
                    }
                    movie.setSerial(isSerialCheckBox.isChecked());
                    DBHelperFactory.getHelper().getMovieTableDAO().update(movie);

                    // Добавление серий
                    if (movie.isSerial()) {
                        if (movieSerial != null) {
                            if (movieSerial.getCurrentSeason() > maxSeasonNumber) {
                                movieSerial.setCurrentSeason(maxSeasonNumber);
                                movieSerial.setCurrentEpisode(1);
                            }
                            movieSerial.setNumberSeasons(maxSeasonNumber);
                            DBHelperFactory.getHelper().getMovieSeasonsTableDAO().delete(movieSerial.getListSeasons());
                            DBHelperFactory.getHelper().getMovieSerialTableDAO().update(movieSerial);
                        } else {
                            movieSerial = new MovieSerial();
                            movieSerial.setParentId(movie.getId());
                            movieSerial.setCurrentSeason(1);
                            movieSerial.setCurrentEpisode(1);
                            movieSerial.setNumberSeasons(maxSeasonNumber);
                            DBHelperFactory.getHelper().getMovieSerialTableDAO().create(movieSerial);
                        }
                        for (MovieSeason movieSeason : movieSeasons) {
                            movieSeason.setParentId(movieSerial.getId());
                            DBHelperFactory.getHelper().getMovieSeasonsTableDAO().create(movieSeason);
                        }
                    }
                    Toast.makeText(getApplicationContext(),
                            getString(R.string.edit_movie_activity_update_movie_toast_text),
                            Toast.LENGTH_SHORT).show();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            onBackPressed();
        });
    }

    @Override
    public Loader<Movie> onCreateLoader(int id, Bundle args) {
        Loader<Movie> loader = null;
        if (id == LOADER_GET_MOVIES_ID) {
            loader = new GetMovieByIdLoader(this, args);
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Movie> loader, Movie data) {
        switch (loader.getId()) {
            case LOADER_GET_MOVIES_ID:
                if (data != null) {
                    movie = data;
                    titleText.setText(data.getTitle());
                    getSupportActionBar().setTitle(
                            String.format(getString(R.string.edit_movie_activity_title_edit_text),
                                    data.getTitle())
                    );
                    overviewText.setText(data.getOverview());
                    runtimeText.setText(String.valueOf(data.getRuntime()));
                    Glide.with(this).load(data.getPoster_w780()).into(poster);

                    switch (data.getCategory()) {
                        case Movie.NAME_CATEGORY_FILM:
                            mediaTypeSpinner.setSelection(0);
                            isSerialCheckBox.setEnabled(false);
                            break;
                        case Movie.NAME_CATEGORY_TV:
                            mediaTypeSpinner.setSelection(1);
                            isSerialCheckBox.setEnabled(false);
                            break;
                        case Movie.NAME_CATEGORY_CARTOON:
                            mediaTypeSpinner.setSelection(2);
                            isSerialCheckBox.setEnabled(true);
                            break;
                        case Movie.NAME_CATEGORY_ANIME:
                            mediaTypeSpinner.setSelection(3);
                            isSerialCheckBox.setEnabled(true);
                            break;
                    }

                    movieSerial = movie.getMovieSerial();
                    if (data.isSerial() && movieSerial != null) {
                        isSerialCheckBox.setChecked(true);
                        movieSeasons = movieSerial.getListSeasons();

                        currentSeasonNumber = movieSerial.getNumberSeasons();
                        maxSeasonNumber = currentSeasonNumber;
                        seasonNumber.setMax(maxSeasonNumber - 1);
                        seasonStopText.setText(String.valueOf(maxSeasonNumber));
                        episodeCount.setMax(getEpisodeCountBySeason() - 1);
                        episodeStopText.setText(String.valueOf(getEpisodeCountBySeason()));
                        serialContainer.setVisibility(View.VISIBLE);

                        seasonNumber.setProgress(currentSeasonNumber - 1);
                        episodeCount.setProgress(getEpisodeCountBySeason() - 1);

                        String strFormat;
                        strFormat = String.format("%s %d",
                                getString(R.string.edit_movie_activity_season_number_label_text),
                                maxSeasonNumber);
                        seasonNumberLabel.setText(strFormat);
                        strFormat = String.format("%s %d",
                                getString(R.string.edit_movie_activity_episode_count_label_text),
                                getEpisodeCountBySeason());
                        episodeCountLabel.setText(strFormat);
                    }
                }

                findViewById(R.id.edit_movie_activity_scroll_content).setVisibility(View.VISIBLE);
                findViewById(R.id.edit_movie_activity_save_btn).setVisibility(View.VISIBLE);
                loadProgressBar.setVisibility(View.GONE);

                // блокировка полей, которые добавлены из API
                if (data.getExId() != 0) {
                    runtimeText.setEnabled(false);
                    overviewText.setEnabled(false);
                }
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Movie> loader) {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private int getEpisodeCountBySeason() {
        if (movieSeasons != null && !movieSeasons.isEmpty() && movieSeasons.size() >= currentSeasonNumber) {
            return movieSeasons.get(currentSeasonNumber - 1).getEpisodeCount();
        }
        return MovieSerial.MAX_NUMBER_EPISODES;
    }
}
