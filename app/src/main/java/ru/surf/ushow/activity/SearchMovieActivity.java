package ru.surf.ushow.activity;

import android.content.Context;
import android.content.Intent;
import android.os.PersistableBundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.support.v7.widget.SearchView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.List;

import ru.surf.ushow.NetworkManager;
import ru.surf.ushow.R;
import ru.surf.ushow.adapter.MovieHorizontalListAdapter;
import ru.surf.ushow.adapter.MoviesListAdapter;
import ru.surf.ushow.loader.GetPopularFilmsLoader;
import ru.surf.ushow.loader.GetPopularTvLoader;
import ru.surf.ushow.loader.SearchMoviesAsyncTaskLoader;
import ru.surf.ushow.model.Movie;
import ru.surf.ushow.model.MoviesContainer;

/**
 * Activity для добавления movie
 */
public class SearchMovieActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<MoviesContainer> {

    private final int LOADER_SEARCH_MOVIES_ID = 1;
    private final int LOADER_GET_POPULAR_FILMS_ID = 2;
    private final int LOADER_GET_POPULAR_TV_ID = 3;
    private final String STATE_QUERY_TEXT = "query_text";

    private TextView emptyMovieListLabel;
    private ProgressBar loadMovieProgressBar;
    private RecyclerView movieListView;
    private RecyclerView filmsHorizontalListView;
    private RecyclerView tvHorizontalListView;
    private SearchView search;
    private LinearLayout filmsListContainer;
    private LinearLayout tvListContainer;
    private CoordinatorLayout searchContainer;
    private Bundle bundle = new Bundle();
    private ScrollView movieListContainer;
    private List<Movie> movies = new ArrayList<>();
    private List<Movie> movieFilms = new ArrayList<>();
    private List<Movie> movieTV = new ArrayList<>();
    private String queryText;

    public static void start(Context context) {
        Intent i = new Intent(context, SearchMovieActivity.class);
        context.startActivity(i);
    }

    // TODO клавиатура обрезает экран при поворачивании
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_movie_activity);

        // устанавливаем toolbar как actionbar (для совместимости)
        setSupportActionBar((Toolbar) findViewById(R.id.search_movie_activity_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.search_movie_activity_title_text));

        loadMovieProgressBar = (ProgressBar) findViewById(R.id.search_movie_activity_load_movie_progressbar);
        emptyMovieListLabel = (TextView) findViewById(R.id.search_movie_activity_empty_movie_list_label);
        movieListContainer = (ScrollView) findViewById(R.id.search_movie_activity_movie_horizontal_list_view_container);
        filmsListContainer = (LinearLayout) findViewById(R.id.search_movie_activity_films_horizontal_list_view_container);
        tvListContainer = (LinearLayout) findViewById(R.id.search_movie_activity_tv_horizontal_list_view_container);
        searchContainer = (CoordinatorLayout) findViewById(R.id.search_movie_activity_search_container);

        if (NetworkManager.isNetworkAvailable(this)) {
            loadMovieProgressBar.setVisibility(View.VISIBLE);
            movieListContainer.setVisibility(View.VISIBLE);
            getSupportLoaderManager().restartLoader(LOADER_GET_POPULAR_FILMS_ID, null, this);
            getSupportLoaderManager().restartLoader(LOADER_GET_POPULAR_TV_ID, null, this);
        } else {
            emptyMovieListLabel.setText(getString(R.string.search_movie_activity_no_connect_movie_list_text));
            emptyMovieListLabel.setVisibility(View.VISIBLE);
        }

        movieListView = (RecyclerView) findViewById(R.id.search_movie_activity_movie_list_view);
        movieListView.setHasFixedSize(true);
        movieListView.setLayoutManager(new LinearLayoutManager(this));
        movieListView.setAdapter(new MoviesListAdapter(movies));

        filmsHorizontalListView = (RecyclerView) findViewById(R.id.search_movie_activity_films_horizontal_list_view);
        fillPopularMoviesWithData(filmsHorizontalListView, movieFilms, "movie", 10);
        TextView showAllFilms = (TextView) findViewById(R.id.search_movie_activity_films_all_label);
        showAllFilms.setOnClickListener(v -> PopularFilmsActivity.start(SearchMovieActivity.this));

        tvHorizontalListView = (RecyclerView) findViewById(R.id.search_movie_activity_tv_horizontal_list_view);
        fillPopularMoviesWithData(tvHorizontalListView, movieTV, "tv", 10);
        TextView showAllTv = (TextView) findViewById(R.id.search_movie_activity_tv_all_label);
        showAllTv.setOnClickListener(v -> PopularTvActivity.start(SearchMovieActivity.this));

        CardView addMovie = (CardView) findViewById(R.id.search_movie_activity_add_button_card);
        RxView.clicks(addMovie).subscribe(aVoid -> {
            EditMovieActivity.start(this, search.getQuery().toString());
        });
    }

    private void fillPopularMoviesWithData(RecyclerView rv, List<Movie> movies, String mediaType, int limit) {
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        MovieHorizontalListAdapter movieHorizontalListAdapter = new MovieHorizontalListAdapter(movies, mediaType);
        movieHorizontalListAdapter.setLimit(limit);
        rv.setAdapter(movieHorizontalListAdapter);
    }

    @Override
    public Loader<MoviesContainer> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LOADER_SEARCH_MOVIES_ID:
                return new SearchMoviesAsyncTaskLoader(this, args);
            case LOADER_GET_POPULAR_FILMS_ID:
                return new GetPopularFilmsLoader(this);
            case LOADER_GET_POPULAR_TV_ID:
                return new GetPopularTvLoader(this);
            default:
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader loader, MoviesContainer data) {
        loadMovieProgressBar.setVisibility(View.GONE);
        switch (loader.getId()) {
            case LOADER_SEARCH_MOVIES_ID:
                movies.clear();
                if (!search.getQuery().toString().isEmpty()) {
                    if (data != null && data.getMovies().size() > 0) {
                        movies.addAll(data.getMovies());
                    }
                }
                movieListView.getAdapter().notifyDataSetChanged();

                if (movies.size() > 0) {
                    movieListView.setVisibility(View.VISIBLE);
                } else if (!search.getQuery().toString().isEmpty()) {
                    movieListView.setVisibility(View.GONE);
                    emptyMovieListLabel.setText(getString(R.string.search_movie_activity_no_find_movie_list_text));
                    emptyMovieListLabel.setVisibility(View.VISIBLE);
                } else {

                }
                break;
            case LOADER_GET_POPULAR_FILMS_ID:
                movieFilms.clear();
                if (data != null && data.getMovies().size() > 0) {
                    movieFilms.addAll(data.getMovies());
                    filmsHorizontalListView.getAdapter().notifyDataSetChanged();
                    filmsListContainer.setVisibility(View.VISIBLE);
                }
                break;
            case LOADER_GET_POPULAR_TV_ID:
                movieTV.clear();
                if (data != null && data.getMovies().size() > 0) {
                    movieTV.addAll(data.getMovies());
                    tvHorizontalListView.getAdapter().notifyDataSetChanged();
                    tvListContainer.setVisibility(View.VISIBLE);
                }
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {
    }

    // TODO на большом экране лупа посередине и не сохраняет искомый текст
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_movie_search_view, menu);

        MenuItem myActionMenuItem = menu.findItem(R.id.search_movie_menu_action_search);
        search = (SearchView) myActionMenuItem.getActionView();
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                emptyMovieListLabel.setVisibility(View.GONE);
                searchContainer.setVisibility(View.GONE);
                movieListContainer.setVisibility(View.GONE);
                if (NetworkManager.isNetworkAvailable(SearchMovieActivity.this)) {
                    if (newText.length() > 0) {
                        loadMovieProgressBar.setVisibility(View.VISIBLE);
                        bundle.putString(SearchMoviesAsyncTaskLoader.TITLE_MOVIE, newText);
                        getSupportLoaderManager().restartLoader(LOADER_SEARCH_MOVIES_ID, bundle, SearchMovieActivity.this);
                        searchContainer.setVisibility(View.VISIBLE);
                    } else {
                        movieListContainer.setVisibility(View.VISIBLE);
                    }
                } else {
                    emptyMovieListLabel.setText(getString(R.string.search_movie_activity_no_connect_movie_list_text));
                    emptyMovieListLabel.setVisibility(View.VISIBLE);
                }
                return false;
            }
        });
        if (queryText != null && !queryText.equals("")) {
            myActionMenuItem.expandActionView();
            search.setQuery(queryText, false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_QUERY_TEXT, search.getQuery().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        queryText = savedInstanceState.getString(STATE_QUERY_TEXT);
    }
}
