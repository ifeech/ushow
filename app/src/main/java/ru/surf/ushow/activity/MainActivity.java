package ru.surf.ushow.activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ru.surf.ushow.DB.DBHelperFactory;
import ru.surf.ushow.R;
import ru.surf.ushow.adapter.MovieHorizontalListAdapterWithMenu;
import ru.surf.ushow.model.Movie;

public class MainActivity extends AppCompatActivity {

    private final int LOADER_TOKEN_ID = 1;

    private LinearLayout filmsListContainer;
    private LinearLayout tvListContainer;
    private LinearLayout cartoonsListContainer;
    private LinearLayout animeListContainer;
    private TextView emptyMovieContainerLabel;
    private List<Movie> movieFilms = new ArrayList<>();
    private List<Movie> movieTV = new ArrayList<>();
    private List<Movie> movieCartoons = new ArrayList<>();
    private List<Movie> movieAnime = new ArrayList<>();
    private RecyclerView filmsHorizontalListView;
    private RecyclerView tvHorizontalListView;
    private RecyclerView cartoonsHorizontalListView;
    private RecyclerView animeHorizontalListView;

    public static void start(Context context) {
        Intent i = new Intent(context, MainActivity.class);
        context.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        // устанавливаем toolbar как actionbar (для совместимости)
        setSupportActionBar((Toolbar) findViewById(R.id.main_activity_toolbar));
        getSupportActionBar().setTitle(getString(R.string.app_name));

        filmsListContainer = (LinearLayout) findViewById(R.id.main_activity_films_horizontal_list_view_container);
        tvListContainer = (LinearLayout) findViewById(R.id.main_activity_tv_horizontal_list_view_container);
        cartoonsListContainer = (LinearLayout) findViewById(R.id.main_activity_cartoons_horizontal_list_view_container);
        animeListContainer = (LinearLayout) findViewById(R.id.main_activity_anime_horizontal_list_view_container);
        emptyMovieContainerLabel = (TextView) findViewById(R.id.main_activity_empty_movie_container_label);

        filmsHorizontalListView = (RecyclerView) findViewById(R.id.main_activity_films_horizontal_list_view);
        filmsHorizontalListView.setHasFixedSize(true);
        filmsHorizontalListView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        filmsHorizontalListView.setAdapter(new MovieHorizontalListAdapterWithMenu(movieFilms));

        tvHorizontalListView = (RecyclerView) findViewById(R.id.main_activity_tv_horizontal_list_view);
        tvHorizontalListView.setHasFixedSize(true);
        tvHorizontalListView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        tvHorizontalListView.setAdapter(new MovieHorizontalListAdapterWithMenu(movieTV));

        cartoonsHorizontalListView = (RecyclerView) findViewById(R.id.main_activity_cartoons_horizontal_list_view);
        cartoonsHorizontalListView.setHasFixedSize(true);
        cartoonsHorizontalListView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        cartoonsHorizontalListView.setAdapter(new MovieHorizontalListAdapterWithMenu(movieCartoons));

        animeHorizontalListView = (RecyclerView) findViewById(R.id.main_activity_anime_horizontal_list_view);
        animeHorizontalListView.setHasFixedSize(true);
        animeHorizontalListView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        animeHorizontalListView.setAdapter(new MovieHorizontalListAdapterWithMenu(movieAnime));

        FloatingActionButton addMovieBtn = (FloatingActionButton) findViewById(R.id.main_activity_add_movie_btn);
        addMovieBtn.setOnClickListener(v -> SearchMovieActivity.start(MainActivity.this));

        // Переход к категории со всеми movie
        TextView filmsAllBtn = (TextView) findViewById(R.id.main_activity_all_films_label);
        filmsAllBtn.setOnClickListener(v -> UserMoviesActivity.startFilms(MainActivity.this));
        TextView tvAllBtn = (TextView) findViewById(R.id.main_activity_all_tv_label);
        tvAllBtn.setOnClickListener(v -> UserMoviesActivity.startTV(MainActivity.this));
        TextView cartoonsAllBtn = (TextView) findViewById(R.id.main_activity_all_cartoons_label);
        cartoonsAllBtn.setOnClickListener(v -> UserMoviesActivity.startCartoons(MainActivity.this));
        TextView animeAllBtn = (TextView) findViewById(R.id.main_activity_all_anime_label);
        animeAllBtn.setOnClickListener(v -> UserMoviesActivity.startAnime(MainActivity.this));
    }

    public void fillMovieListContainerWithData(ViewGroup container, RecyclerView rv, List<Movie> movies, String category) {
        try {
            movies.addAll(DBHelperFactory.getHelper().getMovieTableDAO().getMoviesByCategory(category, 7L));
            rv.getAdapter().notifyDataSetChanged();
            container.setVisibility(movies.isEmpty() ? View.GONE : View.VISIBLE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean hasMovieContainer() {
        return !movieFilms.isEmpty() && !movieTV.isEmpty() && !movieCartoons.isEmpty() && !movieAnime.isEmpty();
    }

    public void setEmptyContainerTextVisible() {
        if (hasMovieContainer()) {
            emptyMovieContainerLabel.setVisibility(View.VISIBLE);
        } else {
            emptyMovieContainerLabel.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        fillMovieListContainerWithData(filmsListContainer, filmsHorizontalListView, movieFilms, Movie.NAME_CATEGORY_FILM);
        fillMovieListContainerWithData(tvListContainer, tvHorizontalListView, movieTV, Movie.NAME_CATEGORY_TV);
        fillMovieListContainerWithData(cartoonsListContainer, cartoonsHorizontalListView, movieCartoons, Movie.NAME_CATEGORY_CARTOON);
        fillMovieListContainerWithData(animeListContainer, animeHorizontalListView, movieAnime, Movie.NAME_CATEGORY_ANIME);

        setEmptyContainerTextVisible();
    }

    @Override
    protected void onStop() {
        super.onStop();

        movieFilms.clear();
        movieTV.clear();
        movieCartoons.clear();
        movieAnime.clear();
    }
}
