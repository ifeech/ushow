package ru.surf.ushow.activity;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.List;

import ru.surf.ushow.NetworkManager;
import ru.surf.ushow.R;
import ru.surf.ushow.adapter.PopularMoviesListAdapter;
import ru.surf.ushow.model.Movie;
import ru.surf.ushow.model.MoviesContainer;

abstract public class AMoreMoviesActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<MoviesContainer> {

    protected List<Movie> movies = new ArrayList<>();
    protected TextView emptyContentLabel;
    protected ProgressBar loadMovieProgressBar;
    protected RecyclerView movieListView;

    protected abstract String getMovieTitle();

    protected abstract String getMediaType();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popular_movie_activity);

        setSupportActionBar((Toolbar) findViewById(R.id.popular_movie_activity_toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getMovieTitle());

        loadMovieProgressBar = (ProgressBar) findViewById(R.id.popular_movie_activity_load_movie_progressbar);
        emptyContentLabel = (TextView) findViewById(R.id.popular_movie_activity_empty_content_label);

        movieListView = (RecyclerView) findViewById(R.id.popular_movie_activity_grid_list_view);
        movieListView.setHasFixedSize(true);
        movieListView.setLayoutManager(new LinearLayoutManager(this));
        movieListView.setAdapter(new PopularMoviesListAdapter(movies, getMediaType()));

        if (NetworkManager.isNetworkAvailable(this)) {
            loadMovieProgressBar.setVisibility(View.VISIBLE);
            getSupportLoaderManager().restartLoader(getLoaderId(), null, this);
        } else {
            emptyContentLabel.setText(R.string.popular_movie_activity_no_connect_label_text);
            emptyContentLabel.setVisibility(View.VISIBLE);
        }

        CardView addMovie = (CardView) findViewById(R.id.popular_movie_activity_add_button_card);
        RxView.clicks(addMovie).subscribe(aVoid -> {
            EditMovieActivity.start(this, "");
        });
    }

    @Override
    public Loader<MoviesContainer> onCreateLoader(int id, Bundle args) {
        if (id == getLoaderId()) {
            return createLoader(id, args);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<MoviesContainer> loader, MoviesContainer data) {
        loadMovieProgressBar.setVisibility(View.GONE);
        if (loader.getId() == getLoaderId()) {
            movies.clear();
            if (data != null && !data.getMovies().isEmpty()) {
                movies.addAll(data.getMovies());
            }
            movieListView.getAdapter().notifyDataSetChanged();

            setListVisibility(data != null && !data.getMovies().isEmpty());
        }
    }

    @Override
    public void onLoaderReset(Loader<MoviesContainer> loader) {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setListVisibility(boolean visible) {
        if (visible) {
            movieListView.setVisibility(View.VISIBLE);
        } else {
            movieListView.setVisibility(View.GONE);
            emptyContentLabel.setText(getEmptyText());
            emptyContentLabel.setVisibility(View.VISIBLE);
        }
    }

    abstract protected String getEmptyText();

    abstract protected int getLoaderId();

    abstract protected Loader<MoviesContainer> createLoader(int id, Bundle args);
}
