package ru.surf.ushow.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.Loader;

import ru.surf.ushow.R;
import ru.surf.ushow.loader.GetPopularFilmsLoader;
import ru.surf.ushow.model.MoviesContainer;

public class PopularFilmsActivity extends AMoreMoviesActivity {

    private final static int LOADER_GET_POPULAR_FILMS_ID = 1;

    private final static String MEDIA_TYPE = "movie";

    public static void start(Context context) {
        Intent i = new Intent(context, PopularFilmsActivity.class);
        context.startActivity(i);
    }

    @Override
    protected int getLoaderId() {
        return LOADER_GET_POPULAR_FILMS_ID;
    }

    @Override
    protected Loader<MoviesContainer> createLoader(int id, Bundle args) {
        return new GetPopularFilmsLoader(this);
    }

    @Override
    protected String getEmptyText() {
        return getString(R.string.popular_movie_activity_empty_films_content_label_text);
    }

    @Override
    protected String getMovieTitle() {
        return getString(R.string.popular_movie_activity_films_title);
    }

    @Override
    protected String getMediaType() {
        return MEDIA_TYPE;
    }
}
