package ru.surf.ushow.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.Loader;

import ru.surf.ushow.R;
import ru.surf.ushow.loader.GetPopularTvLoader;
import ru.surf.ushow.model.MoviesContainer;

public class PopularTvActivity extends AMoreMoviesActivity {

    protected final static int LOADER_GET_POPULAR_TV_ID = 1;

    private final static String MEDIA_TYPE = "tv";

    public static void start(Context context) {
        Intent i = new Intent(context, PopularTvActivity.class);
        context.startActivity(i);
    }

    @Override
    protected int getLoaderId() {
        return LOADER_GET_POPULAR_TV_ID;
    }

    @Override
    protected Loader<MoviesContainer> createLoader(int id, Bundle args) {
        return new GetPopularTvLoader(this);
    }

    @Override
    protected String getMovieTitle() {
        return getString(R.string.popular_movie_activity_tv_title);
    }

    @Override
    protected String getMediaType() {
        return MEDIA_TYPE;
    }

    @Override
    protected String getEmptyText() {
        return getString(R.string.popular_movie_activity_empty_tv_content_label_text);
    }
}
