package ru.surf.ushow.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.sql.SQLException;
import java.util.List;

import ru.surf.ushow.DB.DBHelperFactory;
import ru.surf.ushow.R;
import ru.surf.ushow.activity.MovieDetailActivity;
import ru.surf.ushow.fragments.AUserMoviesFragment;
import ru.surf.ushow.model.Movie;
import ru.surf.ushow.model.MovieSeason;
import ru.surf.ushow.model.MovieSerial;

/**
 * Список пользователя
 */
public class UserMoviesListAdapter extends RecyclerView.Adapter<UserMoviesListAdapter.UserMoviesViewHolder> {

    private List<Movie> movies;

    public UserMoviesListAdapter(List<Movie> obj) {
        this.movies = obj;
    }

    @Override
    public UserMoviesListAdapter.UserMoviesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_movies_list_item, parent, false);
        return new UserMoviesViewHolder(view, parent.getContext());
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(final UserMoviesViewHolder holder, int position) {
        final Movie movie = movies.get(position);
        if (movie != null) {
            final Intent intent = new Intent(AUserMoviesFragment.ACTION_UPDATE_LIST);
            intent.putExtra(AUserMoviesFragment.EXTRA_UPDATE_LIST, true);
            intent.putExtra(AUserMoviesFragment.EXTRA_UPDATE_ITEM, position);
            if (movie.isSerial()) {
                final MovieSerial movieSerial = movie.getMovieSerial();
                String strFormat = String.format("%s%n%d %s %d %s",
                        "Остановился на",
                        movieSerial.getCurrentSeason(),
                        "сезоне",
                        movieSerial.getCurrentEpisode(),
                        "эпизод"
                );
                holder.movieOverview.setText(strFormat);
                if (!movie.isArchive()) {
                    holder.addEpisodeBtn.setVisibility(View.VISIBLE);
                    holder.addEpisodeBtn.setOnClickListener(v -> {
                        updateEpisode(movieSerial.getCurrentEpisode() + 1, holder, movie, movieSerial);
                        LocalBroadcastManager.getInstance(holder.context).sendBroadcast(intent);
                    });
                }
            } else {
                holder.movieOverview.setText(movie.getOverview());
            }
            holder.movieTitle.setText(movie.getTitle());
            if (movie.getRuntime() != 0) {
                String strFormat = String.format("%d %s",
                        movie.getRuntime(),
                        holder.context.getString(R.string.user_movies_list_item_min_label)
                );
                holder.movieRuntime.setText(strFormat);
                holder.favoriteContainer.setVisibility(View.VISIBLE);
            }
            String statusLabel = "";
            if (movie.getAiringYear() != null) {
                statusLabel = statusLabel.concat(movie.getAiringYear() + " | ");
            }
            if (movie.getStatus() != null) {
                statusLabel = statusLabel.concat(movie.getStatus());
            }
            if (statusLabel != null && !statusLabel.equals("")) {
                holder.movieStatus.setText(statusLabel);
                holder.movieStatus.setVisibility(View.VISIBLE);
            }
            Glide.with(holder.context).load(movie.getPoster_w780()).into(holder.moviePoster);
            if (movie.isFavorite()) {
                holder.favoriteBtn.setImageResource(R.drawable.ic_favorite_active);
            } else {
                holder.favoriteBtn.setImageResource(R.drawable.ic_favorite_border_black_no_active);
            }
            holder.cardMovie.setOnClickListener(v -> MovieDetailActivity.startByDB(
                    holder.context,
                    movie.getId(),
                    movie.getTitle(),
                    movie.getPoster_w780()
            ));
            holder.favoriteBtn.setOnClickListener(v -> {
                movie.setFavorite(!movie.isFavorite());
                try {
                    DBHelperFactory.getHelper().getMovieTableDAO().update(movie);
                    LocalBroadcastManager.getInstance(holder.context).sendBroadcast(intent);
                    if (movie.isFavorite()) {
                        ((ImageView) v).setImageResource(R.drawable.ic_favorite_active);
                    } else {
                        ((ImageView) v).setImageResource(R.drawable.ic_favorite_border_black_no_active);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public class UserMoviesViewHolder extends RecyclerView.ViewHolder {

        private Context context;
        private CardView cardMovie;
        private TextView movieTitle;
        private TextView movieOverview;
        private TextView movieRuntime;
        private TextView movieStatus;
        private ImageView moviePoster;
        private ImageView favoriteBtn;
        private ImageView addEpisodeBtn;
        private LinearLayout favoriteContainer;

        public UserMoviesViewHolder(View itemView, Context context) {
            super(itemView);
            this.context = context;
            cardMovie = (CardView) itemView.findViewById(R.id.user_movies_list_item_card_movie);
            movieTitle = (TextView) itemView.findViewById(R.id.user_movies_list_item_movie_title_label);
            movieOverview = (TextView) itemView.findViewById(R.id.user_movies_list_item_movie_overview_label);
            movieRuntime = (TextView) itemView.findViewById(R.id.user_movies_list_item_movie_runtime_label);
            movieStatus = (TextView) itemView.findViewById(R.id.user_movies_list_item_movie_status_label);
            moviePoster = (ImageView) itemView.findViewById(R.id.user_movies_list_item_movie_poster);
            favoriteBtn = (ImageView) itemView.findViewById(R.id.user_movies_list_item_movie_favorite);
            favoriteContainer = (LinearLayout) itemView.findViewById(R.id.user_movies_list_item_movie_runtime_container);
            addEpisodeBtn = (ImageView) itemView.findViewById(R.id.user_movies_list_item_movie_add_episode);

        }
    }

    private int getMaxSeasons(MovieSerial movieSerial) {
        if (movieSerial != null) {
            return movieSerial.getNumberSeasons();
        } else {
            return MovieSerial.MAX_NUMBER_SEASONS;
        }
    }

    // количество эпизодов в сезоне
    private int getEpisodeCountBySeason(MovieSerial movieSerial) {
        MovieSeason movieSeason = null;
        if (movieSerial != null) {
            movieSeason = movieSerial.getListSeasons().get(movieSerial.getCurrentSeason() - 1);
        }
        if (movieSeason != null) {
            return movieSeason.getEpisodeCount();
        } else {
            return MovieSerial.MAX_NUMBER_EPISODES;
        }
    }

    private void updateEpisode(int numberEpisode, UserMoviesViewHolder holder, Movie movie, MovieSerial movieSerial) {
        try {
            if (numberEpisode > getEpisodeCountBySeason(movieSerial)) {
                if (getMaxSeasons(movieSerial) == movieSerial.getCurrentSeason()) {
                    movie.setArchive(true);
                    DBHelperFactory.getHelper().getMovieTableDAO().update(movie);
                    Toast.makeText(holder.context,
                            holder.context.getString(R.string.movie_detail_activity_last_episode_toast_text),
                            Toast.LENGTH_SHORT).show();
                    holder.addEpisodeBtn.setVisibility(View.GONE);
                } else {
                    movieSerial.setCurrentSeason(movieSerial.getCurrentSeason() + 1);
                    movieSerial.setCurrentEpisode(1);
                }
            } else {
                movieSerial.setCurrentEpisode(numberEpisode);
            }

            DBHelperFactory.getHelper().getMovieSerialTableDAO().update(movieSerial);
            String strFormat = String.format("%s%n%d %s %d %s",
                    "Остановился на",
                    movieSerial.getCurrentSeason(),
                    "сезоне",
                    movieSerial.getCurrentEpisode(),
                    "эпизод"
            );
            holder.movieOverview.setText(strFormat);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
