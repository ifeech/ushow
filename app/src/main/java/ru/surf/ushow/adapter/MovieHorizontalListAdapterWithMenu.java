package ru.surf.ushow.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.List;

import ru.surf.ushow.DB.DBHelperFactory;
import ru.surf.ushow.DialogUtil;
import ru.surf.ushow.R;
import ru.surf.ushow.activity.EditMovieActivity;
import ru.surf.ushow.activity.MainActivity;
import ru.surf.ushow.model.Movie;
import ru.surf.ushow.model.MovieSerial;

public class MovieHorizontalListAdapterWithMenu extends MovieHorizontalListAdapter {

    public MovieHorizontalListAdapterWithMenu(List<Movie> obj) {
        super(obj);
    }

    @Override
    public void onBindViewHolder(final MoviesViewHolder holder, final int position) {
        super.onBindViewHolder(holder, position);
        final Movie movie = movies.get(position);

        holder.movieMenu.setVisibility(View.VISIBLE);
        holder.movieMenu.setOnClickListener(v -> {
            final Context context = v.getContext();
            PopupMenu popupMenu = new PopupMenu(context, v);
            popupMenu.inflate(R.menu.movie_horizontal_list_item_menu);

            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.movie_horizontal_list_item_menu_action_edit:
                        EditMovieActivity.startByDB(context, movie.getId());
                        return true;
                    case R.id.movie_horizontal_list_item_menu_action_remove:
                        AlertDialog.Builder removeAlertDialog = DialogUtil.createRemoveMovieDialog(context, (dialog, which) -> {
                            try {
                                MovieSerial movieSerial = movie.getMovieSerial();
                                DBHelperFactory.getHelper().getMovieTableDAO().deleteById(movie.getId());
                                if (movieSerial != null) {
                                    DBHelperFactory.getHelper().getMovieSeasonsTableDAO().delete(movieSerial.getListSeasons());
                                    DBHelperFactory.getHelper().getMovieSerialTableDAO().deleteById(movieSerial.getId());
                                }
                                movies.remove(position);
                                if (movies.size() == 0) {
                                    // TODO скрывать блок с этой категорией
                                }
                                notifyDataSetChanged();

                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                        });
                        removeAlertDialog.show();
                        return true;
                    default:
                        return false;
                }
            });
            popupMenu.show();
        });
    }
}
