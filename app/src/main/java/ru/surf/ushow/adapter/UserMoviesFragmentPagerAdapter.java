package ru.surf.ushow.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import ru.surf.ushow.R;
import ru.surf.ushow.fragments.AllUserMoviesFragment;
import ru.surf.ushow.fragments.ArchiveUserMoviesFragment;
import ru.surf.ushow.fragments.FavoriteUserMoviesFragment;
import ru.surf.ushow.fragments.AUserMoviesFragment;

/**
 * Pager для фрагментов с фильмами пользователя
 */
public class UserMoviesFragmentPagerAdapter extends FragmentPagerAdapter {

    private Fragment[] fragments = new AUserMoviesFragment[]{new FavoriteUserMoviesFragment(), new AllUserMoviesFragment(), new ArchiveUserMoviesFragment()};
    // TODO сделать string
    private List<String> titleFragment = new ArrayList<>();

    public UserMoviesFragmentPagerAdapter(Context context, FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
        titleFragment.add(context.getString(R.string.user_movies_activity_favorite_tab_text));
        titleFragment.add(context.getString(R.string.user_movies_activity_all_tab_text));
        titleFragment.add(context.getString(R.string.user_movies_activity_archive_tab_text));
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titleFragment.get(position);
    }
}
