package ru.surf.ushow.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import ru.surf.ushow.activity.MovieDetailActivity;
import ru.surf.ushow.R;
import ru.surf.ushow.model.Movie;

/**
 * RecycleView для горизонтального вывода movie
 */
public class MovieHorizontalListAdapter extends RecyclerView.Adapter<MovieHorizontalListAdapter.MoviesViewHolder> {

    private int limit;
    private String movieMediaType;
    protected List<Movie> movies;

    public MovieHorizontalListAdapter(List<Movie> obj) {
        this.movies = obj;
    }

    public MovieHorizontalListAdapter(List<Movie> obj, String mediaType) {
        this.movies = obj;
        this.movieMediaType = mediaType;
    }

    @Override
    public MoviesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_horizontal_list_item, parent, false);
        return new MoviesViewHolder(view, parent.getContext());
    }

    @Override
    public void onBindViewHolder(final MoviesViewHolder holder, int position) {
        final Movie movie = movies.get(position);
        if (movie != null) {
            if (TextUtils.isEmpty(movie.getMediaType())) {
                movie.setMediaType(movieMediaType);
            }

            holder.movieTitle.setText(movie.getTitle());
            Glide.with(holder.context).load(movie.getPoster_w780()).placeholder(R.drawable.ic_panorama_black_24dp).into(holder.moviePoster);
            holder.cardMovie.setOnClickListener(v -> {
                if (movie.getId() != 0) {
                    MovieDetailActivity.startByDB(
                            holder.context,
                            movie.getId(),
                            movie.getTitle(),
                            movie.getPoster_w780()
                    );
                } else {
                    MovieDetailActivity.startByApi(
                            holder.context,
                            movie.getExId(),
                            movie.getMediaType(),
                            movie.getTitle(),
                            movie.getPoster_w780()
                    );
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if(limit != 0){
            return limit;
        }
        return movies.size();
    }

    public class MoviesViewHolder extends RecyclerView.ViewHolder {

        private Context context;
        private CardView cardMovie;
        private TextView movieTitle;
        private ImageView moviePoster;
        protected ImageView movieMenu;

        public MoviesViewHolder(View itemView, Context context) {
            super(itemView);
            this.context = context;
            cardMovie = (CardView) itemView.findViewById(R.id.movie_horizontal_list_item_card_movie);
            movieTitle = (TextView) itemView.findViewById(R.id.movie_horizontal_list_item_title_label);
            moviePoster = (ImageView) itemView.findViewById(R.id.movie_horizontal_list_item_poster);
            movieMenu = (ImageView) itemView.findViewById(R.id.movie_horizontal_list_item_menu);
        }
    }

    public void setLimit(int limit){
        this.limit = limit;
    }
}
