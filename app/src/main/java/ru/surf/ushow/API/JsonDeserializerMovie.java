package ru.surf.ushow.API;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import ru.surf.ushow.model.Movie;
import ru.surf.ushow.model.MovieSeason;
import ru.surf.ushow.model.MovieSerial;

/**
 * Парсер детального описания фильма
 */
public class JsonDeserializerMovie implements JsonDeserializer<Movie> {
    @Override
    public Movie deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject object = json.getAsJsonObject();
        JsonElement jsonElement;
        JsonArray jsonArray;
        String mediaType = null;

        if (object.has("media_type")) {
            jsonElement = object.get("media_type");
            if (!jsonElement.isJsonNull()) {
                mediaType = jsonElement.getAsString();
            }
        }

        Movie movie = new Movie();
        if (mediaType != null) {
            movie.setMediaType(mediaType);
        }
        //внешний id
        if (object.has("id")) {
            jsonElement = object.get("id");
            if (!jsonElement.isJsonNull()) {
                movie.setExId(jsonElement.getAsInt());
            }
        }
        //заголовок
        if (object.has("name")) {
            jsonElement = object.get("name");
            if (!jsonElement.isJsonNull()) {
                movie.setTitle(jsonElement.getAsString());
            }
        } else if (object.has("title")) {
            jsonElement = object.get("title");
            if (!jsonElement.isJsonNull()) {
                movie.setTitle(jsonElement.getAsString());
            }
        }
        if (object.has("overview")) {
            jsonElement = object.get("overview");
            if (!jsonElement.isJsonNull()) {
                movie.setOverview(jsonElement.getAsString());
            }
        }
        if (object.has("poster_path")) {
            jsonElement = object.get("poster_path");
            if (!jsonElement.isJsonNull()) {
                movie.setPoster(jsonElement.getAsString());
            }
        }
        if (object.has("status")) {
            jsonElement = object.get("status");
            if (!jsonElement.isJsonNull()) {
                movie.setStatus(jsonElement.getAsString());
            }
        }
        //Продолжительность
        if (object.has("runtime")) {
            jsonElement = object.get("runtime");
            if (!jsonElement.isJsonNull()) {
                movie.setRuntime(jsonElement.getAsInt());
            }
        } else if (object.has("episode_run_time")) {
            jsonArray = object.getAsJsonArray("episode_run_time");
            if (jsonArray.size() != 0) {
                movie.setRuntime(jsonArray.get(0).getAsInt());
            }
        }
        //Дата начала
        if (object.has("release_date")) {
            jsonElement = object.get("release_date");
            if (!jsonElement.isJsonNull()) {
                movie.setAirDate(jsonElement.getAsString());
            }
        } else if (object.has("first_air_date")) {
            jsonElement = object.get("first_air_date");
            if (!jsonElement.isJsonNull()) {
                movie.setAirDate(jsonElement.getAsString());
            }
        }

        // Сезоны
        if (object.has("seasons")) {
            MovieSerial movieSerial = new MovieSerial();
            List<MovieSeason> movieSeasons = new ArrayList<>();
            if (object.has("number_of_seasons")) {
                jsonElement = object.get("number_of_seasons");
                if (!jsonElement.isJsonNull()) {
                    movieSerial.setNumberSeasons(jsonElement.getAsInt());
                }
            }
            // Получение сезонов
            jsonArray = object.getAsJsonArray("seasons");
            for (int i = 0; i < jsonArray.size(); i++) {
                JsonObject seasonObject = jsonArray.get(i).getAsJsonObject();
                if (seasonObject.get("season_number").getAsInt() == 0) continue;
                movieSeasons.add(new MovieSeason(
                        seasonObject.get("episode_count").getAsInt(),
                        seasonObject.get("season_number").getAsInt()));
            }
            if (movieSeasons != null) {
                movieSerial.setListSeasons(movieSeasons);
                movie.setSerial(true);
                movie.setMovieSerial(movieSerial);
            }
        }

        return movie;
    }
}
