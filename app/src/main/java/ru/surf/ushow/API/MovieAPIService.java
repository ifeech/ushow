package ru.surf.ushow.API;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import ru.surf.ushow.model.Movie;
import ru.surf.ushow.model.MoviesContainer;
import ru.surf.ushow.model.Token;

/**
 * API с сайта http://api.themoviedb.org
 */
public interface MovieAPIService {

    public final static String BASE_URL = "http://api.themoviedb.org/3/";
    public final static String API_KEY = "bc119eda5508a66a847d8b0bf2578d1b";

    @GET("authentication/token/new")
    Call<Token> getToken();

    @GET("search/multi")
    Call<MoviesContainer> getMultiMovie(@Query("query") String title);

    @GET("tv/{id}")
    Call<Movie> getTvById(@Path("id") int id);

    @GET("movie/{id}")
    Call<Movie> getFilmById(@Path("id") int id);

    @GET("movie/popular")
    Call<MoviesContainer> getPopularFilms();

    @GET("tv/popular")
    Call<MoviesContainer> getPopularTv();
}