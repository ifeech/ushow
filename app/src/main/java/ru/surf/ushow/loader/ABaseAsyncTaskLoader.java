package ru.surf.ushow.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

/**
 * Базовый loader
 */
abstract public class ABaseAsyncTaskLoader <D> extends AsyncTaskLoader<D> {

    protected D obj;

    public ABaseAsyncTaskLoader(Context context) {
        super(context);
    }

    @Override
    public void deliverResult(D data) {
        if (isReset()) {
            if (data != null) {
                onReleaseResources(data);
            }
        }

        D oldData = obj;
        obj = data;

        if (isStarted()) {
            super.deliverResult(data);
        }

        if (oldData != null) {
            onReleaseResources(oldData);
        }
    }

    @Override
    protected void onStartLoading() {
        if(obj != null){
            deliverResult(obj);
        }
        if (takeContentChanged() || obj == null) {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    public void onCanceled(D data) {
        super.onCanceled(data);
        onReleaseResources(data);
    }

    @Override
    protected void onReset() {
        super.onReset();
        onStopLoading();

        if (obj != null) {
            onReleaseResources(obj);
            obj = null;
        }
    }

    protected abstract void onReleaseResources(D data);
}
