package ru.surf.ushow.loader;

import android.content.Context;
import android.os.Bundle;

import java.sql.SQLException;
import java.util.List;

import ru.surf.ushow.DB.DBHelperFactory;
import ru.surf.ushow.model.Movie;

/**
 * Получение избранных movie из бд
 */
public class GetFavoriteMoviesLoader extends AGetUserMoviesLoader {

    public GetFavoriteMoviesLoader(Context context, Bundle bundle) {
        super(context, bundle);
    }

    @Override
    protected List<Movie> getMoviesByCategory() throws SQLException {
        return DBHelperFactory.getHelper().getMovieTableDAO().getFavoriteMoviesByCategory(movieCategory, 500L);
    }

    @Override
    protected void onReleaseResources(List<Movie> data) {

    }
}
