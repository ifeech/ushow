package ru.surf.ushow.loader;

import android.content.Context;

import retrofit2.Call;
import ru.surf.ushow.API.MovieAPIService;
import ru.surf.ushow.UShowApplication;
import ru.surf.ushow.model.MoviesContainer;

public class GetPopularFilmsLoader extends AGetPopularMovieLoader {

    public GetPopularFilmsLoader(Context context) {
        super(context);
    }

    @Override
    protected void onReleaseResources(MoviesContainer data) {

    }

    @Override
    protected Call<MoviesContainer> call(MovieAPIService movieAPIService) {
        return movieAPIService.getPopularFilms();
    }
}