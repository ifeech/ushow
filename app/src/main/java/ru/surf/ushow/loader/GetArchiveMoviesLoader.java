package ru.surf.ushow.loader;

import android.content.Context;
import android.os.Bundle;

import java.sql.SQLException;
import java.util.List;

import ru.surf.ushow.DB.DBHelperFactory;
import ru.surf.ushow.model.Movie;

/**
 * Получение всех просмотренных movie
 */
public class GetArchiveMoviesLoader extends AGetUserMoviesLoader {

    public GetArchiveMoviesLoader(Context context, Bundle bundle) {
        super(context, bundle);
    }

    @Override
    protected List<Movie> getMoviesByCategory() throws SQLException {
        return DBHelperFactory.getHelper().getMovieTableDAO().getArchiveMoviesByCategory(movieCategory, 500L);
    }

    @Override
    protected void onReleaseResources(List<Movie> data) {

    }
}