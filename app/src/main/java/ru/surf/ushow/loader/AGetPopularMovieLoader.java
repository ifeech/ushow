package ru.surf.ushow.loader;

import android.content.AsyncTaskLoader;
import android.content.Context;

import java.io.IOException;

import retrofit2.Call;
import ru.surf.ushow.API.MovieAPIService;
import ru.surf.ushow.UShowApplication;
import ru.surf.ushow.model.MoviesContainer;

abstract public class AGetPopularMovieLoader extends ABaseAsyncTaskLoader<MoviesContainer> {

    MovieAPIService apiService;

    public AGetPopularMovieLoader(Context context) {
        super(context);
        this.apiService = UShowApplication.getInstance().getMovieApiService();
    }

    @Override
    public MoviesContainer loadInBackground() {
        try {
            obj = call(apiService).execute().body();
            return obj;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    abstract protected Call<MoviesContainer> call(MovieAPIService movieAPIService);
}
