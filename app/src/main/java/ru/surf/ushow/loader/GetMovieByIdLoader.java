package ru.surf.ushow.loader;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import java.io.IOException;
import java.sql.SQLException;

import retrofit2.Call;
import ru.surf.ushow.API.MovieAPIService;
import ru.surf.ushow.DB.DBHelperFactory;
import ru.surf.ushow.UShowApplication;
import ru.surf.ushow.model.Movie;
import ru.surf.ushow.model.MovieSerial;

/**
 * Получить movie по id
 */
public class GetMovieByIdLoader extends ABaseAsyncTaskLoader<Movie> {

    private static final String MOVIE_ID = "movie_id";
    private static final String MOVIE_EX_ID = "movie_ex_id";
    private static final String MOVIE_MEDIA_TYPE = "movie_media_type";
    private static final String MOVIE_PLACE_DATA = "movie_place_data";

    private final int PLACE_DATA_DB = 1;
    private final int PLACE_DATA_API = 2;

    private int idMovie, exIdMovie;
    private int moviePlaceData;
    private String movieMediaType;

    public static Bundle createBundleByAPI(int movieExId, int placeData, String mediaType) {
        Bundle bundle = new Bundle();
        bundle.putString(GetMovieByIdLoader.MOVIE_MEDIA_TYPE, mediaType);
        bundle.putInt(GetMovieByIdLoader.MOVIE_EX_ID, movieExId);
        bundle.putInt(GetMovieByIdLoader.MOVIE_PLACE_DATA, placeData);
        return bundle;
    }

    public static Bundle createBundleByDB(int movieId, int placeData) {
        Bundle bundle = new Bundle();
        bundle.putInt(GetMovieByIdLoader.MOVIE_ID, movieId);
        bundle.putInt(GetMovieByIdLoader.MOVIE_PLACE_DATA, placeData);
        return bundle;
    }

    public GetMovieByIdLoader(Context context, Bundle bundle) {
        super(context);

        if (bundle != null) {
            idMovie = bundle.getInt(MOVIE_ID);
            exIdMovie = bundle.getInt(MOVIE_EX_ID);
            movieMediaType = bundle.getString(MOVIE_MEDIA_TYPE);
            moviePlaceData = bundle.getInt(MOVIE_PLACE_DATA);
        }
        if (TextUtils.isEmpty(movieMediaType)) {
            movieMediaType = null;
        }
    }

    @Override
    public Movie loadInBackground() {
        MovieAPIService movieAPIService;
        Call<Movie> call;

        switch (moviePlaceData) {
            case PLACE_DATA_API:
                movieAPIService = UShowApplication.getInstance().getMovieApiService();
                switch (movieMediaType) {
                    // TODO сделать int константы
                    case "tv":
                        call = movieAPIService.getTvById(exIdMovie);
                        break;
                    case "movie":
                        call = movieAPIService.getFilmById(exIdMovie);
                        break;
                    default:
                        call = null;
                        break;
                }

                if (call != null) {
                    try {
                        obj = call.execute().body();
                        if (movieMediaType.equals("tv")) {
                            obj.setCategory(Movie.NAME_CATEGORY_TV);
                        } else {
                            obj.setCategory(Movie.NAME_CATEGORY_FILM);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case PLACE_DATA_DB:
                try {
                    obj = DBHelperFactory.getHelper().getMovieTableDAO().queryForId(idMovie);
                    // TODO null когда сохр -> ред -> назад -> ред
                    if (obj != null) {
                        MovieSerial movieSerial = DBHelperFactory.getHelper()
                                .getMovieSerialTableDAO()
                                .getMovieSerialByParentId(obj.getId());

                        if (obj.isSerial() && movieSerial != null) {
                            movieSerial.setListSeasons(
                                    DBHelperFactory.getHelper()
                                            .getMovieSeasonsTableDAO()
                                            .getMovieSeasonsByParentId(movieSerial.getId())
                            );
                        }
                        obj.setMovieSerial(movieSerial);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            default:
                return null;
        }

        return obj;
    }

    @Override
    protected void onReleaseResources(Movie data) {
    }
}
