package ru.surf.ushow.loader;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import ru.surf.ushow.API.MovieAPIService;
import ru.surf.ushow.UShowApplication;
import ru.surf.ushow.model.Movie;
import ru.surf.ushow.model.MoviesContainer;

/**
 * Поиск movie по названию
 */
public class SearchMoviesAsyncTaskLoader extends ABaseAsyncTaskLoader<MoviesContainer> {

    public static final String TITLE_MOVIE = "title_movie";

    private String titleMovie;

    public SearchMoviesAsyncTaskLoader(Context context, Bundle bundle) {
        super(context);

        if (bundle != null) {
            titleMovie = bundle.getString(TITLE_MOVIE);
        }
        if (TextUtils.isEmpty(titleMovie)) {
            titleMovie = "";
        }
    }

    @Override
    protected void onReleaseResources(MoviesContainer data) {
    }

    @Override
    public MoviesContainer loadInBackground() {
        MovieAPIService movieAPIService = UShowApplication.getInstance().getMovieApiService();
        Call<MoviesContainer> call = movieAPIService.getMultiMovie(titleMovie);
        try {
            obj = call.execute().body();
            if(obj != null){
                List<Movie> movies = obj.getMovies();
                for (int i = movies.size()-1; i >= 0; i--) {
                    if(movies.get(i).getMediaType().equals("person")){
                        movies.remove(i);
                    }
                }
            }
            return obj;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}