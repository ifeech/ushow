package ru.surf.ushow.loader;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import java.sql.SQLException;
import java.util.List;

import ru.surf.ushow.DB.DBHelperFactory;
import ru.surf.ushow.model.Movie;
import ru.surf.ushow.model.MovieSerial;

abstract public class AGetUserMoviesLoader extends ABaseAsyncTaskLoader<List<Movie>> {

    public static final String MOVIE_CATEGORY = "movie_category";
    protected String movieCategory;

    public AGetUserMoviesLoader(Context context, Bundle bundle) {
        super(context);

        if (bundle != null) {
            movieCategory = bundle.getString(MOVIE_CATEGORY);
        }
        if (TextUtils.isEmpty(movieCategory)) {
            movieCategory = null;
        }
    }

    @Override
    public List<Movie> loadInBackground() {
        try {
            obj = getMoviesByCategory();
            for (Movie movie : obj) {
                if (movie.isSerial()) {
                    MovieSerial movieSerial = DBHelperFactory.getHelper()
                            .getMovieSerialTableDAO()
                            .getMovieSerialByParentId(movie.getId());
                    if (movieSerial != null) {
                        movieSerial.setListSeasons(
                                DBHelperFactory.getHelper()
                                        .getMovieSeasonsTableDAO()
                                        .getMovieSeasonsByParentId(movieSerial.getId())
                        );
                        movie.setMovieSerial(movieSerial);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return obj;
    }

    abstract protected List<Movie> getMoviesByCategory() throws SQLException;
}
