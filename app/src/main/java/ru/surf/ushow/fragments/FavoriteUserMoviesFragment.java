package ru.surf.ushow.fragments;

import android.os.Bundle;
import android.support.v4.content.Loader;

import java.util.List;

import ru.surf.ushow.R;
import ru.surf.ushow.loader.GetFavoriteMoviesLoader;
import ru.surf.ushow.model.Movie;

/**
 * Избранные movie пользователя
 */
public class FavoriteUserMoviesFragment extends AUserMoviesFragment {
    private final static int LOADER_GET_FAVORITE_MOVIES_ID = 1;

    @Override
    protected int getLoaderId() {
        return LOADER_GET_FAVORITE_MOVIES_ID;
    }

    @Override
    protected Loader<List<Movie>> createLoader(int id, Bundle args) {
        return new GetFavoriteMoviesLoader(getContext(), args);
    }

    @Override
    protected String getEmptyText() {
        return getString(R.string.user_movies_fragment_empty_favorite_content_label_text);
    }
}
