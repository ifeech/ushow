package ru.surf.ushow.fragments;

import android.os.Bundle;
import android.support.v4.content.Loader;

import java.util.List;

import ru.surf.ushow.R;
import ru.surf.ushow.loader.GetAllMoviesLoader;
import ru.surf.ushow.model.Movie;

/**
 * Все добавленные видео пользователя (кроме архивных)
 */
public class AllUserMoviesFragment extends AUserMoviesFragment {
    private final static int LOADER_GET_ALL_MOVIES_ID = 1;

    @Override
    protected int getLoaderId() {
        return LOADER_GET_ALL_MOVIES_ID;
    }

    @Override
    protected Loader<List<Movie>> createLoader(int id, Bundle args) {
        return new GetAllMoviesLoader(getContext(), args);
    }

    @Override
    protected String getEmptyText() {
        return getString(R.string.user_movies_fragment_empty_all_content_label_text);
    }
}
