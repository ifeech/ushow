package ru.surf.ushow.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.surf.ushow.R;
import ru.surf.ushow.adapter.UserMoviesListAdapter;
import ru.surf.ushow.loader.AGetUserMoviesLoader;
import ru.surf.ushow.model.Movie;

/**
 * Общий класс для movie пользователя
 */
abstract public class AUserMoviesFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<Movie>> {

    public final static String EXTRA_MOVIE_CATEGORY = "extra_movie_category";
    public final static String EXTRA_UPDATE_LIST = "extra_update_list";
    public final static String EXTRA_UPDATE_ITEM = "extra_update_item";
    public static final String ACTION_UPDATE_LIST = "action_update_list";

    protected List<Movie> movies = new ArrayList<>();
    protected RecyclerView userMoviesList;
    protected ProgressBar loadMovieProgressBar;
    protected TextView emptyContentLabel;
    protected String movieCategory;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra(EXTRA_UPDATE_LIST, false)) {
                updateMoviesList();
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getActivity().getIntent();
        movieCategory = intent.getStringExtra(EXTRA_MOVIE_CATEGORY);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.user_movies_fragment, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(broadcastReceiver, new IntentFilter(ACTION_UPDATE_LIST));
    }

    @Override
    public void onStart() {
        super.onStart();
        switch (movieCategory) {
            case Movie.NAME_CATEGORY_FILM:
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.user_movies_activity_films_title));
                break;
            case Movie.NAME_CATEGORY_TV:
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.user_movies_activity_tv_title));
                break;
            case Movie.NAME_CATEGORY_CARTOON:
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.user_movies_activity_cartoons_title));
                break;
            case Movie.NAME_CATEGORY_ANIME:
                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.user_movies_activity_anime_title));
                break;
        }
        updateMoviesList();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadMovieProgressBar = (ProgressBar) view.findViewById(R.id.user_movies_fragment_load_movies_progress_bar);
        emptyContentLabel = (TextView) view.findViewById(R.id.user_movies_fragment_empty_content_label);

        userMoviesList = (RecyclerView) view.findViewById(R.id.user_movies_fragment_list);
        userMoviesList.setLayoutManager(new LinearLayoutManager(getActivity()));
        userMoviesList.setAdapter(new UserMoviesListAdapter(movies));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(broadcastReceiver);
    }

    @Override
    public Loader<List<Movie>> onCreateLoader(int id, Bundle args) {
        if (id == getLoaderId()) {
            return createLoader(id, args);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<List<Movie>> loader, List<Movie> data) {
        loadMovieProgressBar.setVisibility(View.GONE);
        if (loader.getId() == getLoaderId()) {
            movies.clear();
            if (data != null && data.size() > 0) {
                movies.addAll(data);
            }
            userMoviesList.getAdapter().notifyDataSetChanged();
            //userMoviesList.setAdapter(new UserMoviesListAdapter(movies));

            setListVisibility(data != null && !data.isEmpty());
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Movie>> loader) {
    }

    private void updateMoviesList() {
        loadMovieProgressBar.setVisibility(View.VISIBLE);
        Bundle bundle = new Bundle();
        bundle.putString(AGetUserMoviesLoader.MOVIE_CATEGORY, movieCategory);
        getLoaderManager().restartLoader(getLoaderId(), bundle, this);
    }

    private void setListVisibility(boolean visible) {
        if (visible) {
            emptyContentLabel.setVisibility(View.GONE);
            userMoviesList.setVisibility(View.VISIBLE);
        } else {
            userMoviesList.setVisibility(View.GONE);
            emptyContentLabel.setText(getEmptyText());
            emptyContentLabel.setVisibility(View.VISIBLE);
        }
    }

    abstract protected String getEmptyText();

    abstract protected int getLoaderId();

    abstract protected Loader<List<Movie>> createLoader(int id, Bundle args);
}
