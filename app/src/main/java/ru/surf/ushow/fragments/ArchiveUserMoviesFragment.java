package ru.surf.ushow.fragments;

import android.os.Bundle;
import android.support.v4.content.Loader;

import java.util.List;

import ru.surf.ushow.R;
import ru.surf.ushow.loader.GetArchiveMoviesLoader;
import ru.surf.ushow.model.Movie;

/**
 * Получение всех просмотренных movies из бд
 */
public class ArchiveUserMoviesFragment extends AUserMoviesFragment {
    private final static int LOADER_GET_ARCHIVE_MOVIES_ID = 1;

    @Override
    protected String getEmptyText() {
        return getString(R.string.user_movies_fragment_empty_archive_content_label_text);
    }

    @Override
    protected int getLoaderId() {
        return LOADER_GET_ARCHIVE_MOVIES_ID;
    }

    @Override
    protected Loader<List<Movie>> createLoader(int id, Bundle args) {
        return new GetArchiveMoviesLoader(getContext(), args);
    }
}