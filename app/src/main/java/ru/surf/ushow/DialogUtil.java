package ru.surf.ushow;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class DialogUtil {

    public static AlertDialog.Builder createRemoveMovieDialog(Context context, DialogInterface.OnClickListener positiveClickListener){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle(context.getString(R.string.movie_detail_activity_remove_alert_dialog_title));
        alertDialog.setMessage(context.getString(R.string.movie_detail_activity_remove_alert_dialog_message));
        alertDialog.setPositiveButton(
                context.getString(R.string.movie_detail_activity_remove_alert_dialog_positive_btn_text),
                positiveClickListener
        );
        alertDialog.setNegativeButton(context.getString(
                R.string.movie_detail_activity_remove_alert_dialog_negative_btn_text), (dialog, which) -> {});
        return alertDialog;
    }
}
