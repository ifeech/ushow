package ru.surf.ushow.model;

import com.google.gson.annotations.SerializedName;

/**
 * Модель Token
 */
public class Token {

    @SerializedName(value = "request_token")
    private String token;

    public String getToken(){
        return token;
    }
}
