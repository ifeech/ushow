package ru.surf.ushow.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Список фильмов
 */
public class MoviesContainer {

    @SerializedName(value = "page")
    private int page;
    @SerializedName(value = "results")
    private List<Movie> movies;

    public List<Movie> getMovies(){
        return movies;
    }
}
