package ru.surf.ushow.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.List;

/**
 * Таблица с сериями для movie
 */
@DatabaseTable(tableName = "movie_serial")
public class MovieSerial {

    public final static int MAX_NUMBER_EPISODES = 50;
    public final static int MAX_NUMBER_SEASONS = 50;

    public final static String NAME_FIELD_PARENT_ID = "parent_id";
    public final static String NAME_FIELD_NUMBER_SEASONS = "number_seasons";
    public final static String NAME_FIELD_CURRENT_SEASONS = "current_season";
    public final static String NAME_FIELD_CURRENT_EPISODE = "current_episode";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(columnName = NAME_FIELD_PARENT_ID, unique = true)
    private int parentId;

    @DatabaseField(columnName = NAME_FIELD_NUMBER_SEASONS)
    private int numberSeasons;

    @DatabaseField(columnName = NAME_FIELD_CURRENT_SEASONS)
    private int currentSeason;

    @DatabaseField(columnName = NAME_FIELD_CURRENT_EPISODE)
    private int currentEpisode;

    private List<MovieSeason> seasons = new ArrayList<>();

    public MovieSerial() {
    }

    public int getId() {
        return id;
    }

    public int getNumberSeasons() {
        return numberSeasons;
    }

    public int getCurrentSeason() {
        return currentSeason;
    }

    public int getCurrentEpisode() {
        return currentEpisode;
    }

    public List<MovieSeason> getListSeasons() {
        return seasons;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public void setNumberSeasons(int n) {
        this.numberSeasons = n;
    }

    public void setCurrentSeason(int season) {
        this.currentSeason = season;
    }

    public void setCurrentEpisode(int episode) {
        this.currentEpisode = episode;
    }

    public void setListSeasons(List<MovieSeason> seasons) {
        this.seasons = seasons;
    }
}
