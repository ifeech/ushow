package ru.surf.ushow.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Сезоны movie
 */

@DatabaseTable(tableName = "movie_seasons")
public class MovieSeason {

    public final static String NAME_FIELD_PARENT_ID = "parent_id";
    public final static String NAME_FIELD_SEASON_NUMBER = "season_number";
    public final static String NAME_FIELD_EPISODE_COUNT = "episode_count";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(columnName = NAME_FIELD_PARENT_ID)
    private int parentId;

    @DatabaseField(columnName = NAME_FIELD_EPISODE_COUNT)
    private int episodeCount;

    @DatabaseField(columnName = NAME_FIELD_SEASON_NUMBER)
    private int seasonNumber;

    public MovieSeason() {
    }

    public MovieSeason(int episodeCount, int seasonNumber) {
        this.episodeCount = episodeCount;
        this.seasonNumber = seasonNumber;
    }

    public int getParentId() {
        return parentId;
    }

    public int getEpisodeCount() {
        return episodeCount;
    }

    public int getSeasonNumber() {
        return seasonNumber;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public void setEpisodeCount(int episodeCount) {
        this.episodeCount = episodeCount;
    }

    public void setSeasonNumber(int seasonNumber) {
        this.seasonNumber = seasonNumber;
    }
}
