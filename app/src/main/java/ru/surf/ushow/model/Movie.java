package ru.surf.ushow.model;

import android.text.TextUtils;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Модель элемента фильма в списке
 */
@DatabaseTable(tableName = "movies")
public class Movie {

    private final String URL_IMG_W780 = "https://image.tmdb.org/t/p/w780";
    public final static String URL_POSTER_DEFAULT = "http://huge-it.com/wp-content/plugins/lightbox/images/No-image-found.jpg";
    //public final static String URL_POSTER_DEFAULT = "http://cdn.shopify.com/s/files/1/0231/7685/t/3/assets/no-image-available.png?2277136199072451917";

    public final static String NAME_CATEGORY_FILM = "film";
    public final static String NAME_CATEGORY_TV = "tv";
    public final static String NAME_CATEGORY_CARTOON = "cartoon";
    public final static String NAME_CATEGORY_ANIME = "anime";

    public final static String NAME_FIELD_ID = "id";
    public final static String NAME_FIELD_TITLE = "title";
    public final static String NAME_FIELD_POSTER = "poster";
    public final static String NAME_FIELD_OVERVIEW = "overview";
    public final static String NAME_FIELD_EX_ID = "ex_id";
    public final static String NAME_FIELD_RUNTIME = "runtime";
    public final static String NAME_FIELD_CATEGORY = "category";
    public final static String NAME_FIELD_STATUS = "status";
    public final static String NAME_FIELD_AIR_DATE = "air_date";
    public final static String NAME_FIELD_FAVORITE = "favorite";
    public final static String NAME_FIELD_ARCHIVE = "archive";

    public final static String NAME_FIELD_SERIAL = "serial";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(columnName = NAME_FIELD_EX_ID)
    private int exId;

    @DatabaseField(columnName = NAME_FIELD_RUNTIME)
    private int runtime;

    @DatabaseField(canBeNull = false, columnName = NAME_FIELD_TITLE)
    private String title;

    @DatabaseField(columnName = NAME_FIELD_OVERVIEW)
    private String overview;

    @DatabaseField(columnName = NAME_FIELD_POSTER)
    private String poster;

    @DatabaseField(columnName = NAME_FIELD_CATEGORY, defaultValue = NAME_CATEGORY_FILM)
    private String category;

    @DatabaseField(columnName = NAME_FIELD_STATUS)
    private String status;

    @DatabaseField(columnName = NAME_FIELD_AIR_DATE)
    private String airDate;

    @DatabaseField(columnName = NAME_FIELD_SERIAL)
    private boolean serial;

    @DatabaseField(columnName = NAME_FIELD_FAVORITE)
    private boolean favorite;

    @DatabaseField(columnName = NAME_FIELD_ARCHIVE)
    private boolean archive;

    private String mediaType;

    private MovieSerial movieSerial;

    public Movie() {
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getOverview() {
        return overview;
    }

    public String getMediaType() {
        return mediaType;
    }

    public String getCategory() {
        return category;
    }

    public int getExId() {
        return exId;
    }

    public int getRuntime() {
        return runtime;
    }

    public String getStatus() {
        return status;
    }

    public String getAiringYear() {
        if (!TextUtils.isEmpty(airDate)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy", Locale.getDefault());
            Date date = null;
            try {
                date = dateFormat.parse(airDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return dateFormat.format(date) + " г.";
        }
        return airDate;
    }

    public String getAirDate() {
        return airDate;
    }

    public MovieSerial getMovieSerial() {
        return movieSerial;
    }

    public boolean isSerial() {
        return serial;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public boolean isArchive() {
        return archive;
    }

    public void setAirDate(String airDate) {
        this.airDate = airDate;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setSerial(boolean serial) {
        this.serial = serial;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public void setArchive(boolean archive) {
        this.archive = archive;
    }

    // TODO разобраться с постерами (доб. в бд и вывод)
    public String getPoster_w780() {
        if (poster == null) {
            poster = URL_POSTER_DEFAULT;
        }
        return poster;
    }
    /*public String getPoster_w780() {
        return URL_IMG_W780 + poster;
    }*/

    public void setExId(int id) {
        this.exId = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPoster(String poster) {

        this.poster = URL_IMG_W780 + poster;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }

    public void setMovieSerial(MovieSerial movieSerial) {
        this.movieSerial = movieSerial;
    }

    public boolean hasDescription() {
        return !TextUtils.isEmpty(overview) || runtime != 0 || !TextUtils.isEmpty(status) || !TextUtils.isEmpty(airDate);
    }
}
